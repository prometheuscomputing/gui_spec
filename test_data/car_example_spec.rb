covered_packages "Automotive", "Geography", "People", "Gui_Builder_Profile", "People::Clown"

homepage_item :'Automotive::RepairShop', :getter => 'repair_shops'
homepage_item :'Automotive::VIN', :getter => 'vins'
homepage_item :'Automotive::Warranty', :getter => 'warranties'
homepage_item :'Geography::Country', :getter => 'countries'
homepage_item :'People::Person', :getter => 'people'
homepage_item :'Automotive::Component', :getter => 'components'
homepage_item :'Automotive::Vehicle', :getter => 'vehicles'

organizer(:Details, Home) {
  view_ref(:Summary, 'repair_shops', :label => 'Repair Shops')
  view_ref(:Summary, 'vins', :label => 'VINs')
  view_ref(:Summary, 'warranties', :label => 'Warranties')
  view_ref(:Summary, 'countries', :label => 'Countries')
  view_ref(:Summary, 'people', :label => 'People')
  view_ref(:Summary, 'components', :label => 'Components')
  view_ref(:Summary, 'vehicles', :label => 'Vehicles')
}


# Gui_Builder_Profile::File ====================================================
summary_view(Gui_Builder_Profile::File) {
  string('filename', :label => 'Filename')
  string('mime_type', :label => 'Mime Type')
}
detail_view(Gui_Builder_Profile::File) {
  attributes_from(Gui_Builder_Profile::File)
  view_ref(:Summary, 'binary_data', Gui_Builder_Profile::BinaryData, :label => 'Binary Data')
}

# Gui_Builder_Profile::Code ====================================================
summary_view(Gui_Builder_Profile::Code) {
  choice('language', :label => 'Language')
  string('content', :label => 'Content')
}
detail_view(Gui_Builder_Profile::Code) {
  attributes_from(Gui_Builder_Profile::Code)
}

# Gui_Builder_Profile::RichText ================================================
summary_view(Gui_Builder_Profile::RichText) {
  string('content', :label => 'Content')
  choice('markup_language', :label => 'Markup Language')
}
detail_view(Gui_Builder_Profile::RichText) {
  attributes_from(Gui_Builder_Profile::RichText)
  view_ref(:Summary, 'images', Gui_Builder_Profile::RichTextImage, :label => 'Images')
}

# Automotive::HasWarranty ======================================================
summary_view(Automotive::HasWarranty) {
  date('warranty_expiration_date', :label => 'Warranty Expiration Date')
}
detail_view(Automotive::HasWarranty) {
  attributes_from(Automotive::HasWarranty)
}

# Automotive::HasSerial ========================================================
summary_view(Automotive::HasSerial) {
  integer('serial', :label => 'Serial')
}
detail_view(Automotive::HasSerial) {
  attributes_from(Automotive::HasSerial)
}

# Geography::Location ==========================================================
summary_view(Geography::Location) {
  string('name', :label => 'Name')
}
detail_view(Geography::Location) {
  attributes_from(Geography::Location)
  view_ref(:Summary, 'repair_shops', Automotive::RepairShop, :label => 'Repair Shops')
}

# Automotive::Sponsor ==========================================================
summary_view(Automotive::Sponsor) {
  string('value', :label => 'Value')
}
detail_view(Automotive::Sponsor) {
  attributes_from(Automotive::Sponsor)
  view_ref(:Summary, 'electric_vehicles_with_sponsor', Automotive::ElectricVehicle, :label => 'Electric Vehicles With Sponsors')
}

# Automotive::VehicleMaker =====================================================
summary_view(Automotive::VehicleMaker) {
  string('value', :label => 'Value')
}
detail_view(Automotive::VehicleMaker) {
  attributes_from(Automotive::VehicleMaker)
  view_ref(:Summary, 'vehicles_with_make', Automotive::Vehicle, :label => 'Vehicles With Makes')
}

# Automotive::PaymentFrequency =================================================
summary_view(Automotive::PaymentFrequency) {
  string('value', :label => 'Value')
}
detail_view(Automotive::PaymentFrequency) {
  attributes_from(Automotive::PaymentFrequency)
  view_ref(:Summary, 'ownerships_with_payment_frequency', People::Ownership, :label => 'Ownerships With Payment Frequencies')
}

# People::Clown::ClownAffiliations =============================================
summary_view(People::Clown::ClownAffiliations) {
  string('value', :label => 'Value')
}
detail_view(People::Clown::ClownAffiliations) {
  attributes_from(People::Clown::ClownAffiliations)
  view_ref(:Summary, 'clowns_with_affiliations', People::Clown::Clown, :label => 'Clowns With Affiliations')
}

# People::Clown::DollTraits ====================================================
summary_view(People::Clown::DollTraits) {
  string('value', :label => 'Value')
}
detail_view(People::Clown::DollTraits) {
  attributes_from(People::Clown::DollTraits)
  view_ref(:Summary, 'nesting_dolls_with_traits', People::Clown::NestingDoll, :label => 'Nesting Dolls With Traits')
}

# People::Handedness ===========================================================
summary_view(People::Handedness) {
  string('value', :label => 'Value')
}
detail_view(People::Handedness) {
  attributes_from(People::Handedness)
  view_ref(:Summary, 'people_with_handedness', People::Person, :label => 'People With Handednesses')
}

# Gui_Builder_Profile::FacadeImplementationLanguages ===========================
summary_view(Gui_Builder_Profile::FacadeImplementationLanguages) {
  string('value', :label => 'Value')
}
detail_view(Gui_Builder_Profile::FacadeImplementationLanguages) {
  attributes_from(Gui_Builder_Profile::FacadeImplementationLanguages)
}

# Gui_Builder_Profile::LanguageType ============================================
summary_view(Gui_Builder_Profile::LanguageType) {
  string('value', :label => 'Value')
  code('codes_with_language', :label => 'Codes With Languages', :multiple => true)
}
detail_view(Gui_Builder_Profile::LanguageType) {
  attributes_from(Gui_Builder_Profile::LanguageType)
}

# Gui_Builder_Profile::UserRegistrationType ====================================
summary_view(Gui_Builder_Profile::UserRegistrationType) {
  string('value', :label => 'Value')
}
detail_view(Gui_Builder_Profile::UserRegistrationType) {
  attributes_from(Gui_Builder_Profile::UserRegistrationType)
  view_ref(:Summary, 'project_options_with_user_registration', Gui_Builder_Profile::ProjectOptions, :label => 'Project Options With User Registrations')
}

# Gui_Builder_Profile::RoleRegistrationType ====================================
summary_view(Gui_Builder_Profile::RoleRegistrationType) {
  string('value', :label => 'Value')
}
detail_view(Gui_Builder_Profile::RoleRegistrationType) {
  attributes_from(Gui_Builder_Profile::RoleRegistrationType)
  view_ref(:Summary, 'user_roles_with_registration', Gui_Builder_Profile::UserRole, :label => 'User Roles With Registrations')
}

# Gui_Builder_Profile::MarkupType ==============================================
summary_view(Gui_Builder_Profile::MarkupType) {
  string('value', :label => 'Value')
  text('rich_texts_with_markup_language', :label => 'Rich Texts With Markup Languages', :multiple => true)
}
detail_view(Gui_Builder_Profile::MarkupType) {
  attributes_from(Gui_Builder_Profile::MarkupType)
}

# Automotive::RepairShop =======================================================
summary_view(Automotive::RepairShop) {
  string('name', :label => 'Name')
}
detail_view(Automotive::RepairShop) {
  attributes_from(Automotive::RepairShop)
  view_ref(:Summary, 'location', Geography::Location, :label => 'Location')
  view_ref(:Summary, 'mechanics', Automotive::Mechanic, :label => 'Mechanics')
  view_ref(:Summary, 'chief_mechanic', Automotive::Mechanic, :label => 'Chief Mechanic')
  view_ref(:Summary, 'customers', People::Person, :label => 'Customers')
  view_ref(:Summary, 'currently_working_on', Automotive::Vehicle, :label => 'Currently Working Ons', :orderable => true)
}

# Automotive::VehicleTaxRate ===================================================
summary_view(Automotive::VehicleTaxRate) {
  number('sales_tax_rate', :label => 'Sales Tax Rate')
  integer('property_tax_rate', :label => 'Property Tax Rate')
}
detail_view(Automotive::VehicleTaxRate) {
  attributes_from(Automotive::VehicleTaxRate)
  view_ref(:Summary, 'for_country', Geography::Country, :label => 'For Country')
}

# Automotive::VIN ==============================================================
summary_view(Automotive::VIN) {
  date('issue_date', :label => 'Issue Date')
  integer('vin', :label => 'Vin')
}
detail_view(Automotive::VIN) {
  attributes_from(Automotive::VIN)
  view_ref(:Summary, 'vehicle', Automotive::Vehicle, :label => 'Vehicle')
}

# Automotive::Warranty =========================================================
summary_view(Automotive::Warranty) {
  string('coverage', :label => 'Coverage')
}
detail_view(Automotive::Warranty) {
  attributes_from(Automotive::Warranty)
  view_ref(:Summary, 'warrantieds', Automotive::Warrantied, :label => 'Warrantieds')
  view_ref(:Summary, 'replacement', Automotive::Warrantied, :label => 'Replacement')
}

# Automotive::Part =============================================================
summary_view(Automotive::Part) {
  attributes_from(Automotive::HasSerial)
  string('name', :label => 'Name')
}
detail_view(Automotive::Part) {
  content_from(Automotive::HasSerial)
  attributes_from(Automotive::Part)
  view_ref(:Summary, 'component', Automotive::Component, :label => 'Component')
  view_ref(:Summary, 'sub_parts', Automotive::Part, :label => 'Sub Parts')
  view_ref(:Summary, 'part_of', Automotive::Part, :label => 'Part Of')
}

# Automotive::Repair_Workflow ==================================================
summary_view(Automotive::Repair_Workflow) {
  string('project_name', :label => 'Project Name')
}
detail_view(Automotive::Repair_Workflow) {
  attributes_from(Automotive::Repair_Workflow)
}

# Automotive::ElectricVehicleSponsor_SponsorElectricVehiclesWithSponsor ========
summary_view(Automotive::ElectricVehicleSponsor_SponsorElectricVehiclesWithSponsor) {
}
detail_view(Automotive::ElectricVehicleSponsor_SponsorElectricVehiclesWithSponsor) {
  attributes_from(Automotive::ElectricVehicleSponsor_SponsorElectricVehiclesWithSponsor)
}

# Automotive::VehicleMake_VehicleMakerVehiclesWithMake =========================
summary_view(Automotive::VehicleMake_VehicleMakerVehiclesWithMake) {
}
detail_view(Automotive::VehicleMake_VehicleMakerVehiclesWithMake) {
  attributes_from(Automotive::VehicleMake_VehicleMakerVehiclesWithMake)
}

# Automotive::PersonRepairShops_RepairShopCustomers ============================
summary_view(Automotive::PersonRepairShops_RepairShopCustomers) {
}
detail_view(Automotive::PersonRepairShops_RepairShopCustomers) {
  attributes_from(Automotive::PersonRepairShops_RepairShopCustomers)
}

# Automotive::PersonMaintains_VehicleMaintainedBy ==============================
summary_view(Automotive::PersonMaintains_VehicleMaintainedBy) {
}
detail_view(Automotive::PersonMaintains_VehicleMaintainedBy) {
  attributes_from(Automotive::PersonMaintains_VehicleMaintainedBy)
}

# Geography::State =============================================================
summary_view(Geography::State) {
  string('name', :label => 'Name')
}
detail_view(Geography::State) {
  attributes_from(Geography::State)
  view_ref(:Summary, 'country', Geography::Country, :label => 'Country')
  view_ref(:Summary, 'cities', Geography::City, :label => 'Cities')
}

# Geography::Address ===========================================================
summary_view(Geography::Address) {
  string('street_name', :label => 'Street Name')
  integer('street_number', :label => 'Street Number')
}
detail_view(Geography::Address) {
  attributes_from(Geography::Address)
  view_ref(:Summary, 'city', Geography::City, :label => 'City')
  view_ref(:Summary, 'registered_vehicles', Automotive::Vehicle, :label => 'Registered Vehicles')
  view_ref(:Summary, 'person', People::Person, :label => 'Person')
}

# Geography::Country ===========================================================
summary_view(Geography::Country) {
  attributes_from(Geography::Location)
}
detail_view(Geography::Country) {
  content_from(Geography::Location)
  attributes_from(Geography::Country)
  view_ref(:Summary, 'vehicle_tax_rate', Automotive::VehicleTaxRate, :label => 'Vehicle Tax Rate')
  view_ref(:Summary, 'states', Geography::State, :label => 'States')
  view_ref(:Summary, 'territories', Geography::Territory, :label => 'Territories')
}

# Geography::City ==============================================================
summary_view(Geography::City) {
  attributes_from(Geography::Location)
}
detail_view(Geography::City) {
  content_from(Geography::Location)
  attributes_from(Geography::City)
  # view_ref(:Founding, 'founder', People::Person, :label => 'Founder')
  view_ref(:Summary, 'addresses', Geography::Address, :label => 'Addresses')
  view_ref(:Summary, 'state', Geography::State, :label => 'State')
  view_ref(:Summary, 'territory', Geography::Territory, :label => 'Territory')
}
# summary_view(Geography::City, :view_name => :Founding) {
#   attributes_from(Geography::Location)
#   attributes_from(Geography::City)
#   date('date', :label => 'Date', :association_class_widget => true, :table_name => :people_foundings)
# }

# Geography::Territory =========================================================
summary_view(Geography::Territory) {
  string('name', :label => 'Name')
}
detail_view(Geography::Territory) {
  attributes_from(Geography::Territory)
  view_ref(:Summary, 'cities', Geography::City, :label => 'Cities')
  view_ref(:Summary, 'country', Geography::Country, :label => 'Country')
}

# People::Clown::NestingDoll ===================================================
summary_view(People::Clown::NestingDoll) {
  string('color', :label => 'Color')
  choice('traits', :label => 'Traits', :multiple => true)
}
detail_view(People::Clown::NestingDoll) {
  attributes_from(People::Clown::NestingDoll)
  view_ref(:Summary, 'inner_doll', People::Clown::NestingDoll, :label => 'Inner Doll')
  view_ref(:Summary, 'outer_doll', People::Clown::NestingDoll, :label => 'Outer Doll')
  view_ref(:Summary, 'clown', People::Clown::Clown, :label => 'Clown')
}

# People::Clown::ClownAffiliations_ClownAffiliationsClownsWithAffiliations =====
summary_view(People::Clown::ClownAffiliations_ClownAffiliationsClownsWithAffiliations) {
}
detail_view(People::Clown::ClownAffiliations_ClownAffiliationsClownsWithAffiliations) {
  attributes_from(People::Clown::ClownAffiliations_ClownAffiliationsClownsWithAffiliations)
}

# People::Clown::DollTraitsNestingDollsWithTraits_NestingDollTraits ============
summary_view(People::Clown::DollTraitsNestingDollsWithTraits_NestingDollTraits) {
}
detail_view(People::Clown::DollTraitsNestingDollsWithTraits_NestingDollTraits) {
  attributes_from(People::Clown::DollTraitsNestingDollsWithTraits_NestingDollTraits)
}

# People::Driving ==============================================================
summary_view(People::Driving) {
  boolean('likes_driving', :label => 'Likes Driving')
  text('car_review', :label => 'Car Review')
}
detail_view(People::Driving) {
  attributes_from(People::Driving)
  view_ref(:Summary, 'drife', Automotive::Vehicle, :label => 'Drife')
  view_ref(:Summary, 'driver', People::Person, :label => 'Driver')
}

# People::Person ===============================================================
summary_view(People::Person) {
  string('name', :label => 'Name')
  text('description', :label => 'Description')
  file('photo', :label => 'Photo')
  date('date_of_birth', :label => 'Date Of Birth')
  timestamp('last_updated', :label => 'Last Updated')
  time('wakes_at', :label => 'Wakes At')
  integer('family_size', :label => 'Family Size')
  fixedpoint('weight', :label => 'Weight')
  boolean('dependent', :label => 'Dependent')
  choice('handedness', :label => 'Handedness')
  string('manifesto', :label => 'Manifesto')
  integer('lucky_numbers', :label => 'Lucky Numbers', :multiple => true)
  text('notes', :label => 'Notes')
  string('aliases', :label => 'Aliases', :multiple => true)
}
detail_view(People::Person) {
  attributes_from(People::Person)
  view_ref(:Summary, 'maintains', Automotive::Vehicle, :label => 'Maintains', :orderable => true)
  # view_ref(:Founding, 'founded', Geography::City, :label => 'Founded')
  view_ref(:Ownership, 'vehicles', Automotive::Vehicle, :label => 'Vehicles')
  view_ref(:Driving, 'drives', Automotive::Vehicle, :label => 'Drives', :orderable => true)
  view_ref(:Summary, 'repair_shops', Automotive::RepairShop, :label => 'Repair Shops')
  view_ref(:Summary, 'occupying', Automotive::Vehicle, :label => 'Occupying')
  view_ref(:Summary, 'addresses', Geography::Address, :label => 'Addresses')
}
# summary_view(People::Person, :view_name => :Founding) {
#   attributes_from(People::Person)
#   date('date', :label => 'Date', :association_class_widget => true, :table_name => :people_foundings)
# }
summary_view(People::Person, :view_name => :Ownership) {
  attributes_from(People::Person)
  integer('percent_ownership', :label => 'Percent Ownership', :association_class_widget => true, :table_name => :people_ownerships)
  choice('payment_frequency', :label => 'Payment Frequency', :association_class_widget => true, :table_name => :people_ownerships)
}
summary_view(People::Person, :view_name => :Driving) {
  attributes_from(People::Person)
  boolean('likes_driving', :label => 'Likes Driving', :association_class_widget => true, :table_name => :people_drivings)
  text('car_review', :label => 'Car Review', :association_class_widget => true, :table_name => :people_drivings)
}

# People::Ownership ============================================================
summary_view(People::Ownership) {
  integer('percent_ownership', :label => 'Percent Ownership')
  choice('payment_frequency', :label => 'Payment Frequency')
}
detail_view(People::Ownership) {
  attributes_from(People::Ownership)
  view_ref(:Summary, 'purchased_components', Automotive::Component, :label => 'Purchased Components')
  view_ref(:Summary, 'owner', People::Person, :label => 'Owner')
  view_ref(:Summary, 'vehicle', Automotive::Vehicle, :label => 'Vehicle')
}

# People::Founding =============================================================
# summary_view(People::Founding) {
#   date('date', :label => 'Date')
# }
# detail_view(People::Founding) {
#   attributes_from(People::Founding)
#   view_ref(:Summary, 'founded', Geography::City, :label => 'Founded')
#   view_ref(:Summary, 'founder', People::Person, :label => 'Founder')
# }

# People::HandednessPeopleWithHandedness_PersonHandedness ======================
summary_view(People::HandednessPeopleWithHandedness_PersonHandedness) {
}
detail_view(People::HandednessPeopleWithHandedness_PersonHandedness) {
  attributes_from(People::HandednessPeopleWithHandedness_PersonHandedness)
}

# People::Person_LuckyNumbers ==================================================
summary_view(People::Person_LuckyNumbers) {
  integer('lucky_number', :label => 'Lucky Number')
}
detail_view(People::Person_LuckyNumbers) {
  attributes_from(People::Person_LuckyNumbers)
  view_ref(:Summary, 'person_with_lucky_numbers', People::Person, :label => 'Person With Lucky Numbers')
}

# People::Person_Aliases =======================================================
summary_view(People::Person_Aliases) {
  string('alias', :label => 'Alias')
}
detail_view(People::Person_Aliases) {
  attributes_from(People::Person_Aliases)
  view_ref(:Summary, 'person_with_aliases', People::Person, :label => 'Person With Aliases')
}

# People::OwnershipPaymentFrequency_PaymentFrequencyOwnershipsWithPaymentFrequency 
summary_view(People::OwnershipPaymentFrequency_PaymentFrequencyOwnershipsWithPaymentFrequency) {
}
detail_view(People::OwnershipPaymentFrequency_PaymentFrequencyOwnershipsWithPaymentFrequency) {
  attributes_from(People::OwnershipPaymentFrequency_PaymentFrequencyOwnershipsWithPaymentFrequency)
}

# Gui_Builder_Profile::InvitationCode ==========================================
summary_view(Gui_Builder_Profile::InvitationCode) {
  string('code', :label => 'Code')
  timestamp('expires', :label => 'Expires')
  integer('uses_remaining', :label => 'Uses Remaining')
}
detail_view(Gui_Builder_Profile::InvitationCode) {
  attributes_from(Gui_Builder_Profile::InvitationCode)
  view_ref(:Summary, 'perspectives', Gui_Builder_Profile::Perspective, :label => 'Perspectives')
  view_ref(:GrantedPermissions, 'roles_granted', Gui_Builder_Profile::UserRole, :label => 'Roles Granteds')
  view_ref(:Summary, 'additional_email_requirements', Gui_Builder_Profile::StringCondition, :label => 'Additional Email Requirements')
  view_ref(:Summary, 'organizations', Gui_Builder_Profile::Organization, :label => 'Organizations')
}
summary_view(Gui_Builder_Profile::InvitationCode, :view_name => :GrantedPermissions) {
  attributes_from(Gui_Builder_Profile::InvitationCode)
  boolean('role_manager', :label => 'Role Manager', :association_class_widget => true, :table_name => :gui_builder_profile_granted_permissions)
}

# Gui_Builder_Profile::ProjectOptions ==========================================
summary_view(Gui_Builder_Profile::ProjectOptions) {
  choice('user_registration', :label => 'User Registration')
}
detail_view(Gui_Builder_Profile::ProjectOptions) {
  attributes_from(Gui_Builder_Profile::ProjectOptions)
  view_ref(:Summary, 'email_requirements', Gui_Builder_Profile::StringCondition, :label => 'Email Requirements')
  view_ref(:Summary, 'password_requirements', Gui_Builder_Profile::StringCondition, :label => 'Password Requirements')
}

# Gui_Builder_Profile::StringCondition =========================================
summary_view(Gui_Builder_Profile::StringCondition) {
  string('failure_message', :label => 'Failure Message')
  string('description', :label => 'Description')
}
detail_view(Gui_Builder_Profile::StringCondition) {
  attributes_from(Gui_Builder_Profile::StringCondition)
  view_ref(:Summary, 'email_requirement_for', Gui_Builder_Profile::ProjectOptions, :label => 'Email Requirement For')
  view_ref(:Summary, 'password_requirement_for', Gui_Builder_Profile::ProjectOptions, :label => 'Password Requirement For')
  view_ref(:Summary, 'additional_email_requirement_for', Gui_Builder_Profile::InvitationCode, :label => 'Additional Email Requirement For')
}

# Gui_Builder_Profile::Perspective =============================================
summary_view(Gui_Builder_Profile::Perspective) {
  string('name', :label => 'Name')
}
detail_view(Gui_Builder_Profile::Perspective) {
  attributes_from(Gui_Builder_Profile::Perspective)
  view_ref(:Summary, 'invitation_codes', Gui_Builder_Profile::InvitationCode, :label => 'Invitation Codes')
  view_ref(:Summary, 'users', Gui_Builder_Profile::User, :label => 'Users')
  view_ref(:Summary, 'substitutions', Gui_Builder_Profile::MultivocabularySubstitution, :label => 'Substitutions')
}

# Gui_Builder_Profile::UserRole ================================================
summary_view(Gui_Builder_Profile::UserRole) {
  string('name', :label => 'Name')
  choice('registration', :label => 'Registration')
}
detail_view(Gui_Builder_Profile::UserRole) {
  attributes_from(Gui_Builder_Profile::UserRole)
  view_ref(:RolePermissions, 'users', Gui_Builder_Profile::User, :label => 'Users')
  view_ref(:GrantedPermissions, 'invitations', Gui_Builder_Profile::InvitationCode, :label => 'Invitations')
}
summary_view(Gui_Builder_Profile::UserRole, :view_name => :RolePermissions) {
  attributes_from(Gui_Builder_Profile::UserRole)
  boolean('role_manager', :label => 'Role Manager', :association_class_widget => true, :table_name => :gui_builder_profile_role_permissions)
}
summary_view(Gui_Builder_Profile::UserRole, :view_name => :GrantedPermissions) {
  attributes_from(Gui_Builder_Profile::UserRole)
  boolean('role_manager', :label => 'Role Manager', :association_class_widget => true, :table_name => :gui_builder_profile_granted_permissions)
}

# Gui_Builder_Profile::GrantedPermissions ======================================
summary_view(Gui_Builder_Profile::GrantedPermissions) {
  boolean('role_manager', :label => 'Role Manager')
}
detail_view(Gui_Builder_Profile::GrantedPermissions) {
  attributes_from(Gui_Builder_Profile::GrantedPermissions)
  view_ref(:Summary, 'roles_granted', Gui_Builder_Profile::UserRole, :label => 'Roles Granted')
  view_ref(:Summary, 'invitation', Gui_Builder_Profile::InvitationCode, :label => 'Invitation')
}

# Gui_Builder_Profile::RolePermissions =========================================
summary_view(Gui_Builder_Profile::RolePermissions) {
  boolean('role_manager', :label => 'Role Manager')
}
detail_view(Gui_Builder_Profile::RolePermissions) {
  attributes_from(Gui_Builder_Profile::RolePermissions)
  view_ref(:Summary, 'role', Gui_Builder_Profile::UserRole, :label => 'Role')
  view_ref(:Summary, 'user', Gui_Builder_Profile::User, :label => 'User')
}

# Gui_Builder_Profile::MultivocabularySubstitution =============================
summary_view(Gui_Builder_Profile::MultivocabularySubstitution) {
  string('master_word', :label => 'Master Word')
  string('replacement_word', :label => 'Replacement Word')
}
detail_view(Gui_Builder_Profile::MultivocabularySubstitution) {
  attributes_from(Gui_Builder_Profile::MultivocabularySubstitution)
  view_ref(:Summary, 'perspective', Gui_Builder_Profile::Perspective, :label => 'Perspective')
}

# Gui_Builder_Profile::BinaryData ==============================================
summary_view(Gui_Builder_Profile::BinaryData) {
  file('file', :label => 'File')

}
detail_view(Gui_Builder_Profile::BinaryData) {
  attributes_from(Gui_Builder_Profile::BinaryData)
}

# Gui_Builder_Profile::RegularExpressionCondition ==============================
summary_view(Gui_Builder_Profile::RegularExpressionCondition) {
  attributes_from(Gui_Builder_Profile::StringCondition)
  string('regular_expression', :label => 'Regular Expression')
}
detail_view(Gui_Builder_Profile::RegularExpressionCondition) {
  content_from(Gui_Builder_Profile::StringCondition)
  attributes_from(Gui_Builder_Profile::RegularExpressionCondition)
}

# Gui_Builder_Profile::RichTextImage ===========================================
summary_view(Gui_Builder_Profile::RichTextImage) {
  text('rich_text', :label => 'Rich Text')
  file('image', :label => 'Image')
}
detail_view(Gui_Builder_Profile::RichTextImage) {
  attributes_from(Gui_Builder_Profile::RichTextImage)
}

# Gui_Builder_Profile::Person ==================================================
summary_view(Gui_Builder_Profile::Person) {
  string('first_name', :label => 'First Name')
  string('last_name', :label => 'Last Name')
  string('email', :label => 'Email')
  boolean('email_verified', :label => 'Email Verified')
}
detail_view(Gui_Builder_Profile::Person) {
  attributes_from(Gui_Builder_Profile::Person)
  view_ref(:Summary, 'organizations', Gui_Builder_Profile::Organization, :label => 'Organizations')
}

# Gui_Builder_Profile::Organization ============================================
summary_view(Gui_Builder_Profile::Organization) {
  string('name', :label => 'Name')
  text('description', :label => 'Description')
  integer('org_user_limit', :label => 'Org User Limit')
}
detail_view(Gui_Builder_Profile::Organization) {
  attributes_from(Gui_Builder_Profile::Organization)
  view_ref(:Summary, 'people', Gui_Builder_Profile::Person, :label => 'People')
  view_ref(:Summary, 'invitation_codes', Gui_Builder_Profile::InvitationCode, :label => 'Invitation Codes')
}

# Gui_Builder_Profile::ProjectOptionsUserRegistration_UserRegistrationTypeProjectOptionsWithUserRegistration 
summary_view(Gui_Builder_Profile::ProjectOptionsUserRegistration_UserRegistrationTypeProjectOptionsWithUserRegistration) {
}
detail_view(Gui_Builder_Profile::ProjectOptionsUserRegistration_UserRegistrationTypeProjectOptionsWithUserRegistration) {
  attributes_from(Gui_Builder_Profile::ProjectOptionsUserRegistration_UserRegistrationTypeProjectOptionsWithUserRegistration)
}

# Gui_Builder_Profile::RoleRegistrationTypeUserRolesWithRegistration_UserRoleRegistration 
summary_view(Gui_Builder_Profile::RoleRegistrationTypeUserRolesWithRegistration_UserRoleRegistration) {
}
detail_view(Gui_Builder_Profile::RoleRegistrationTypeUserRolesWithRegistration_UserRoleRegistration) {
  attributes_from(Gui_Builder_Profile::RoleRegistrationTypeUserRolesWithRegistration_UserRoleRegistration)
}

# Gui_Builder_Profile::CodeLanguage_LanguageTypeCodesWithLanguage ==============
summary_view(Gui_Builder_Profile::CodeLanguage_LanguageTypeCodesWithLanguage) {
}
detail_view(Gui_Builder_Profile::CodeLanguage_LanguageTypeCodesWithLanguage) {
  attributes_from(Gui_Builder_Profile::CodeLanguage_LanguageTypeCodesWithLanguage)
}

# Gui_Builder_Profile::MarkupTypeRichTextsWithMarkupLanguage_RichTextMarkupLanguage 
summary_view(Gui_Builder_Profile::MarkupTypeRichTextsWithMarkupLanguage_RichTextMarkupLanguage) {
}
detail_view(Gui_Builder_Profile::MarkupTypeRichTextsWithMarkupLanguage_RichTextMarkupLanguage) {
  attributes_from(Gui_Builder_Profile::MarkupTypeRichTextsWithMarkupLanguage_RichTextMarkupLanguage)
}

# Gui_Builder_Profile::InvitationCodePerspectives_PerspectiveInvitationCodes ===
summary_view(Gui_Builder_Profile::InvitationCodePerspectives_PerspectiveInvitationCodes) {
}
detail_view(Gui_Builder_Profile::InvitationCodePerspectives_PerspectiveInvitationCodes) {
  attributes_from(Gui_Builder_Profile::InvitationCodePerspectives_PerspectiveInvitationCodes)
}

# Gui_Builder_Profile::InvitationCodeOrganizations_OrganizationInvitationCodes =
summary_view(Gui_Builder_Profile::InvitationCodeOrganizations_OrganizationInvitationCodes) {
}
detail_view(Gui_Builder_Profile::InvitationCodeOrganizations_OrganizationInvitationCodes) {
  attributes_from(Gui_Builder_Profile::InvitationCodeOrganizations_OrganizationInvitationCodes)
}

# Gui_Builder_Profile::PerspectiveUsers_UserPerspectives =======================
summary_view(Gui_Builder_Profile::PerspectiveUsers_UserPerspectives) {
}
detail_view(Gui_Builder_Profile::PerspectiveUsers_UserPerspectives) {
  attributes_from(Gui_Builder_Profile::PerspectiveUsers_UserPerspectives)
}

# Gui_Builder_Profile::OrganizationPeople_PersonOrganizations ==================
summary_view(Gui_Builder_Profile::OrganizationPeople_PersonOrganizations) {
}
detail_view(Gui_Builder_Profile::OrganizationPeople_PersonOrganizations) {
  attributes_from(Gui_Builder_Profile::OrganizationPeople_PersonOrganizations)
}

# Automotive::Warrantied =======================================================
summary_view(Automotive::Warrantied) {
  attributes_from(Automotive::HasWarranty)
  attributes_from(Automotive::HasSerial)
  boolean('warranty_void', :label => 'Warranty Void')
}
detail_view(Automotive::Warrantied) {
  content_from(Automotive::HasWarranty)
  content_from(Automotive::HasSerial)
  attributes_from(Automotive::Warrantied)
  view_ref(:Summary, 'warranty', Automotive::Warranty, :label => 'Warranty')
  view_ref(:Summary, 'replacement_for', Automotive::Warranty, :label => 'Replacement For')
}

# Automotive::DomesticVehicleMaker =============================================
summary_view(Automotive::DomesticVehicleMaker) {
  attributes_from(Automotive::VehicleMaker)
  string('value', :label => 'Value')
}
detail_view(Automotive::DomesticVehicleMaker) {
  content_from(Automotive::VehicleMaker)
  attributes_from(Automotive::DomesticVehicleMaker)
}

# Automotive::ForeignVehicleMaker ==============================================
summary_view(Automotive::ForeignVehicleMaker) {
  attributes_from(Automotive::VehicleMaker)
  string('value', :label => 'Value')
}
detail_view(Automotive::ForeignVehicleMaker) {
  content_from(Automotive::VehicleMaker)
  attributes_from(Automotive::ForeignVehicleMaker)
}

# Automotive::OtherVehicleMaker ================================================
summary_view(Automotive::OtherVehicleMaker) {
  attributes_from(Automotive::DomesticVehicleMaker)
  attributes_from(Automotive::ForeignVehicleMaker)
  string('value', :label => 'Value')
}
detail_view(Automotive::OtherVehicleMaker) {
  content_from(Automotive::DomesticVehicleMaker)
  content_from(Automotive::ForeignVehicleMaker)
  attributes_from(Automotive::OtherVehicleMaker)
}

# Automotive::Component ========================================================
summary_view(Automotive::Component) {
  attributes_from(Automotive::Warrantied)
  string('name', :label => 'Name')
}
detail_view(Automotive::Component) {
  content_from(Automotive::Warrantied)
  attributes_from(Automotive::Component)
  view_ref(:Summary, 'vehicle', Automotive::Vehicle, :label => 'Vehicle')
  view_ref(:Summary, 'parts', Automotive::Part, :label => 'Parts')
  view_ref(:Summary, 'ownership', People::Ownership, :label => 'Ownership')
}

# Automotive::Mechanic =========================================================
summary_view(Automotive::Mechanic) {
  attributes_from(People::Person)
  integer('salary', :label => 'Salary')
}
detail_view(Automotive::Mechanic) {
  content_from(People::Person)
  attributes_from(Automotive::Mechanic)
  view_ref(:Summary, 'employer', Automotive::RepairShop, :label => 'Employer')
  view_ref(:Summary, 'chief_for', Automotive::RepairShop, :label => 'Chief For')
}

# Automotive::Vehicle ==========================================================
summary_view(Automotive::Vehicle) {
  attributes_from(Automotive::Warrantied)
  choice('make', :label => 'Make')
  string('vehicle_model', :label => 'Vehicle Model')
  integer('cost', :label => 'Cost')
}
detail_view(Automotive::Vehicle) {
  content_from(Automotive::Warrantied)
  attributes_from(Automotive::Vehicle)
  view_ref(:Summary, 'maintained_by', People::Person, :label => 'Maintained By', :orderable => true)
  view_ref(:Summary, 'registered_at', Geography::Address, :label => 'Registered At')
  view_ref(:Summary, 'components', Automotive::Component, :label => 'Components')
  view_ref(:Ownership, 'owners', People::Person, :label => 'Owners')
  view_ref(:Driving, 'drivers', People::Person, :label => 'Drivers', :orderable => true)
  view_ref(:Summary, 'vin', Automotive::VIN, :label => 'VIN')
  view_ref(:Summary, 'occupants', People::Person, :label => 'Occupants', :orderable => true)
  view_ref(:Summary, 'being_repaired_by', Automotive::RepairShop, :label => 'Being Repaired By')
}
summary_view(Automotive::Vehicle, :view_name => :Ownership) {
  attributes_from(Automotive::Warrantied)
  attributes_from(Automotive::Vehicle)
  integer('percent_ownership', :label => 'Percent Ownership', :association_class_widget => true, :table_name => :people_ownerships)
  choice('payment_frequency', :label => 'Payment Frequency', :association_class_widget => true, :table_name => :people_ownerships)
}
summary_view(Automotive::Vehicle, :view_name => :Driving) {
  attributes_from(Automotive::Warrantied)
  attributes_from(Automotive::Vehicle)
  boolean('likes_driving', :label => 'Likes Driving', :association_class_widget => true, :table_name => :people_drivings)
  text('car_review', :label => 'Car Review', :association_class_widget => true, :table_name => :people_drivings)
}

# Automotive::Car ==============================================================
summary_view(Automotive::Car) {
  attributes_from(Automotive::Vehicle)
  integer('miles_per_gallon', :label => 'Miles Per Gallon')
  string('state', :label => 'State')
  boolean('inspection_completed', :label => 'Inspection Completed')
}
detail_view(Automotive::Car) {
  content_from(Automotive::Vehicle)
  attributes_from(Automotive::Car)
}

# People::Clown::Unicycle ======================================================
summary_view(People::Clown::Unicycle) {
  attributes_from(Automotive::Vehicle)
  string('color', :label => 'Color')
}
detail_view(People::Clown::Unicycle) {
  content_from(Automotive::Vehicle)
  attributes_from(People::Clown::Unicycle)
  view_ref(:Summary, 'clown', People::Clown::Clown, :label => 'Clown')
}

# People::Clown::Clown =========================================================
summary_view(People::Clown::Clown) {
  attributes_from(People::Person)
  string('clown_name', :label => 'Clown Name')
  choice('affiliations', :label => 'Affiliations', :multiple => true)
}
detail_view(People::Clown::Clown) {
  content_from(People::Person)
  attributes_from(People::Clown::Clown)
  view_ref(:Summary, 'unicycles', People::Clown::Unicycle, :label => 'Unicycles')
  view_ref(:Summary, 'dolls', People::Clown::NestingDoll, :label => 'Dolls')
}

# Gui_Builder_Profile::User ====================================================
summary_view(Gui_Builder_Profile::User) {
  attributes_from(Gui_Builder_Profile::Person)
  string('login', :label => 'Login')
  string('password_hash', :label => 'Password Hash')
  string('salt', :label => 'Salt')
  boolean('use_accessibility', :label => 'Use Accessibility')
  string('password_reset_token', :label => 'Password Reset Token')
  timestamp('password_reset_time_limit', :label => 'Password Reset Time Limit')
  string('email_confirmation_token', :label => 'Email Confirmation Token')
}
detail_view(Gui_Builder_Profile::User) {
  content_from(Gui_Builder_Profile::Person)
  attributes_from(Gui_Builder_Profile::User)
  view_ref(:RolePermissions, 'roles', Gui_Builder_Profile::UserRole, :label => 'Roles')
  view_ref(:Summary, 'perspectives', Gui_Builder_Profile::Perspective, :label => 'Perspectives')
}
summary_view(Gui_Builder_Profile::User, :view_name => :RolePermissions) {
  attributes_from(Gui_Builder_Profile::Person)
  attributes_from(Gui_Builder_Profile::User)
  boolean('role_manager', :label => 'Role Manager', :association_class_widget => true, :table_name => :gui_builder_profile_role_permissions)
}

# Automotive::Minivan ==========================================================
summary_view(Automotive::Minivan) {
  attributes_from(Automotive::Car)
  boolean('sliding_doors', :label => 'Sliding Doors')
}
detail_view(Automotive::Minivan) {
  content_from(Automotive::Car)
  attributes_from(Automotive::Minivan)
}

# Automotive::ElectricVehicle ==================================================
summary_view(Automotive::ElectricVehicle) {
  attributes_from(Automotive::Vehicle)
  integer('electric_efficiency', :label => 'Electric Efficiency')
  choice('sponsor', :label => 'Sponsor')
}
detail_view(Automotive::ElectricVehicle) {
  content_from(Automotive::Vehicle)
  attributes_from(Automotive::ElectricVehicle)
}

# Automotive::HybridVehicle ====================================================
summary_view(Automotive::HybridVehicle) {
  attributes_from(Automotive::Car)
  attributes_from(Automotive::ElectricVehicle)
  string('hybrid_type', :label => 'Hybrid Type')
}
detail_view(Automotive::HybridVehicle) {
  content_from(Automotive::Car)
  content_from(Automotive::ElectricVehicle)
  attributes_from(Automotive::HybridVehicle)
}

# Automotive::Motorcycle =======================================================
summary_view(Automotive::Motorcycle) {
  attributes_from(Automotive::Vehicle)
}
detail_view(Automotive::Motorcycle) {
  content_from(Automotive::Vehicle)
  attributes_from(Automotive::Motorcycle)
}

