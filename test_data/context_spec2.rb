covered_packages 'Geography', 'People'
# covered_packages "Automotive", "Geography", "People", "Gui_Builder_Profile", "People::Clown"

# - test multiple contexts referring to same object
organizer(:Details,  People::Person) do
  string('name', label:'Name')
  
  context('founded', :auto_create => false) do
    view_ref(:Summary, 'addresses', :label => 'Addresses in Founded City')
  end
  
  # Note that this gets the same obj as the above context but this one includes :auto_create => true
  context('founded', :auto_create => true) do
    view_ref(:Summary, 'state', :label => 'State Where Person Founded a City')
  end
  
  # TODO
  # - test to see if attributes of context widget are shown or if it is just a "pointer" to the different context
  # - - if just a pointer, why is a view_name needed????
end

organizer(:Summary, Geography::State) do
  string('name')
end

organizer(:Summary, Geography::City) do
  string('name')
end

collection(:Summary, Geography::Address) do
  integer('street_number')
  string('street_name')
end
