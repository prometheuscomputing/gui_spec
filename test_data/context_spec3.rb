covered_packages 'Geography', 'People', 'Automotive'

# - test indirectly nested contexts
organizer(:Details,  People::Person) do
  string('name', label:'Name')
  
  context('founded', :auto_create => false) do
    view_ref(:StateToCountry, 'state', :label => 'State to Country')
  end
  
  # TODO
  # - test to see if attributes of context widget are shown or if it is just a "pointer" to the different context
  # - - if just a pointer, why is a view_name needed????
  # - test nested context switches
end

organizer(:StateToCountry, Geography::State) do
  context('country', :auto_create => false) do
    string('name')
    view_ref(:Summary, Automotive::VehicleTaxRate, :label => 'Vehicle Tax Rate')
  end
end

organizer(:Summary, Geography::City) do
  string('name')
end

organizer(:Summary, Automotive::VehicleTaxRate) do
  float('sales_tax_rate')
end
