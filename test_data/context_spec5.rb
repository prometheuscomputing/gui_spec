covered_packages 'Geography', 'People', 'Automotive'

# - test circularly nested contexts
organizer(:Details,  People::Person) do
  string('name', label:'Name')
  
  context('founded', :auto_create => false) do
    context('founder', :auto_create => false) do
      context('founded', :auto_create => false) do
        context('founder', :auto_create => false) do
          string('name', label:'Nested Name')
        end
      end
    end
  end
  
  # TODO
  # - test to see if attributes of context widget are shown or if it is just a "pointer" to the different context
  # - - if just a pointer, why is a view_name needed????
end

organizer(:Summary, Geography::State) do
  string('name')
end

organizer(:Summary, People::Person) do
  string('name')
end
