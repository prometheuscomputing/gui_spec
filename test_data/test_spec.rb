covered_packages "Automotive", "Geography", "People", "Gui_Builder_Profile", "People::Clown"

homepage_item :'Automotive::RepairShop', :getter => 'repair_shops'

collection(:PeopleCollection, People::Person) {
  text 'description'
}

organizer(:PeopleOrganizer, People::Person) {
  ckeditor 'description'
}

global_spec_modifier do |view|
  view.hide('Inspection Completed')
  view.hide('vin')
end


organizer(:SmallSummary, Automotive::Car) {
  # attributes_from(Automotive::Vehicle)
  integer('miles_per_gallon', :label => 'Miles Per Gallon')
  string('state', :label => 'State')
  boolean('inspection_completed', :label => 'Inspection Completed')
  integer('vin')
}

organizer(:SmallSummary, Automotive::HybridVehicle) {
  inherits_from_spec(nil, Automotive::Car, nil, true)
  reorder('vin', :to_beginning => true)
}

collection(:SummaryExplicitCollection, Automotive::Car) {
  integer('miles_per_gallon', :label => 'Miles Per Gallon')
  string('state', :label => 'State')
  boolean('inspection_completed', :label => 'Inspection Completed')
  integer('vin') 
}
