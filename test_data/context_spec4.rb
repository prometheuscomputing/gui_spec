covered_packages 'People', 'Automotive'

# - test directly nested contexts
organizer(:Details,  People::Person) do
  string('name', label:'Name')
  
  context('founded', :auto_create => false) do
    context('state', :auto_create => false) do
      string('name')
    end
  end
  
  # TODO
  # - test to see if attributes of context widget are shown or if it is just a "pointer" to the different context
  # - - if just a pointer, why is a view_name needed????
  # - test nested context switches
end

organizer(:Summary, Geography::State) do
  string('name')
end

organizer(:Summary, Geography::City) do
  string('name')
end
