covered_packages 'Geography', 'People'
# covered_packages "Automotive", "Geography", "People", "Gui_Builder_Profile", "People::Clown"

organizer(:Details,  People::Person) do
  # string('name', label:'Name')
  view_ref(:Summary, 'founded', :label => 'VR Founded')
  
  # Simple Case of context use
  context('founded', :auto_create => false) do
    view_ref(:Summary, 'addresses', :label => 'Addresses in Founded City (Context Simple Test)')
  end
  
  # TODO
  # - test multiple contexts referring to same object
  # - test to see if attributes of context widget are shown or if it is just a "pointer" to the different context
  # - - if just a pointer, why is a view_name needed????
  # - test nested context switches
end

organizer(:Summary, Geography::City) do
  string('name')
end

collection(:Summary, Geography::Address) do
  integer('street_number')
  string('street_name')
end
