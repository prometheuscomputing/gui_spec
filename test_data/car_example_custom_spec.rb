# People::Person ===========================================
organizer(:Details, People::Person) {
  button('download_info', :label => 'Download Serialized Information', :button_text => 'Download Info', :display_result => :file)
  text('description', :label => 'Description', :show_language_selection => true)
  reorder("Download Serialized Information", :after => "Name")
  # test disable_if and hide_if
  is_robot = proc { |arg| arg.name =~ /robot/i }
  disable_if(is_robot, 'description', 'photo', 'date_of_birth', 'last_updated', 'wakes_at', 'family_size', 'weight', 'dependent', 'handedness', 'manifesto', 'lucky_numbers', 'aliases', 'addresses', 'drives', 'maintains', 'occupying', 'repair_shops', 'vehicles', 'download_info')
  # Proc is multi-line in order to make sure multi-line procs are OK with generated site
  is_ghost = proc do |arg|
    arg.name =~ /ghost/i
  end
  hide_if(is_ghost, 'description', 'photo', 'date_of_birth', 'last_updated', 'wakes_at', 'family_size', 'weight', 'dependent', 'handedness', 'manifesto', 'lucky_numbers', 'aliases', 'addresses', 'drives', 'maintains', 'occupying', 'repair_shops', 'vehicles', 'download_info', 'founded')
  hide_if(:is_huge?, 'photo')

  # This is testing the ability to pass the vertex into the proc.  I could not really think of a really good 'domain real' reason to do this within car example but you'll have to trust that such reasons really do exist in other projects.  This does demonstrate the capability to manipulate page behavior based on information about widgets.
  getter_is_name = proc do |person_obj, widget|
    if n = person_obj.name
      n.downcase.gsub(/\s+/, '_') == widget.getter.to_s
    end
  end
  hide_if(getter_is_name, 'description', 'photo', 'date_of_birth', 'last_updated', 'wakes_at', 'family_size', 'weight', 'dependent', 'handedness', 'manifesto', 'lucky_numbers', 'aliases', 'addresses', 'drives', 'maintains', 'occupying', 'repair_shops', 'vehicles', 'download_info')

  # Tests of filtering -- Can't use 'Vehicles' in the labels because it causes Capybara/cukes to whine and cry
  view_ref(:Driving,'drives_expensive', :label => 'Bling Rides', :orderable => true)
  view_ref(:Summary,'electric_vehicles', :label => 'Electric Jalopies', :orderable => true)
  
  html(:label => 'HTML Widget Test', :html => proc{
    haml = <<-EOS
      %input#test{:type => 'hidden', :value => 'test'}
      %div
        %h3
          This is a test of the HTML widget on an object page.
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  })
}
organizer(:Details, Automotive::Mechanic) {
  inherits_from_spec(nil,People::Person,nil,true)
}
organizer(:Details, People::Clown::Clown) {
  inherits_from_spec(nil,People::Person,nil,true)
}

collection(:Bobs, People::Person, :page_size => 100, :filter_value => {:case_insensitive_like => {'name' => 'bob'}}) {
  inherits_from_spec(:Summary, People::Person, :Collection, false)
}

# ----------------------------------------------------------------

organizer(:Details, Automotive::ElectricVehicle) {
  reorder('vehicle_model', :to_beginning => true)
}

organizer(:Details, Automotive::HybridVehicle) {
  reorder('Drivers', :to_beginning => true)
}

organizer(:Details, Automotive::Minivan) {
  # TODO: relabel should not reorder
  relabel("Vehicle Model", "Model")
  hide("Replacement For")
  order("Make", "Model", "Cost", "Sliding Doors", "Miles Per Gallon", "Owners", "Drivers", "Occupants", "Maintained By",
    "Being Repaired By", "Components", "Warranty", "Warranty Expiration Date", "Warranty Void", "Serial", "VIN", "Registered At")
}


# ----------------------------------------------------------------
# Enabling things that were disabled in the spec when they should not have been.  This is due to bad code in specGen
organizer(:Details, People::Clown::NestingDoll) {
  enable('inner_doll', 'outer_doll')
}

organizer(:Details, Automotive::Part) {
  enable('part_of')
}

# ----------------------------------------------------------------
organizer(:Details, People::Driving) {
  relabel('Drife', 'Vehicle')
}


# For testing disabling of creation / deletion / addition / removal
organizer(:Details, Automotive::VehicleTaxRate) {
  disable_removal('for_country')
  disable_creation('for_country')
  disable_addition('for_country')
  disable_deletion('for_country')
  disable_traversal('for_country')
}

# to-one
organizer(:Details, Automotive::Warranty) {
  disable_creation('warrantieds')
}

# to-many (containment)
organizer(:Details, Geography::Country) {
  disable_traversal('territories')
  disable_deletion('territories')
}

# to-one
organizer(:Details, Automotive::RepairShop) {
  disable_traversal('chief_mechanic')
  disable_addition('customers')
  disable_removal('mechanics')
}

# to-one
organizer(:Details, Automotive::Car) {
  disable_deletion('vin')
}


# Example hex_integer widget in organizer
organizer(:HexVIN, Automotive::VIN) {
  date('issue_date', :label => 'Issue Date')
  hex_integer('vin', :label => 'Hex VIN')
}
# Example hex_integer widget in collection
collection(:HexFamilySize, People::Person){
  string('name', :label => 'Name')
  hex_integer('family_size', :label => 'Hex Family Size')
}
# Altered Car view to use HexFamilySize collection
organizer(:HexFamilySizeVehicle, Automotive::Car) {
  inherits_from_spec(:Details,Automotive::Car,nil,true)
  view_ref(:HexFamilySize, 'owners', :label => 'Owners')
}

# NOTE: this doesn't work due to data_classifier somehow being derived from the view_classifier.
#       All views within this organizer fail to populate because of this bug.
#       I'm leaving this here until the problem is fixed.
#       The workaround is to always specify exact view classifiers. -SD
# Altered Vehicle view to use HexFamilySize collection
# organizer(:HexFamilySizeVehicle, Automotive::Vehicle) {
#   inherits_from_spec(:Details,Automotive::Vehicle,nil,true)
#   view_ref(:HexFamilySize, 'owners', :label => 'Owners')
# }


homepage_item :'Gui_Builder_Profile::User'
homepage_item :'People::Person', :getter => 'bobs'
homepage_item :'Automotive::Mechanic' # just used to test hiding
homepage_item :'Geography::City' # just used to test disabling

organizer(:Details, Home) {
  view_ref(:Summary, 'users', :label => 'Users')
  view_ref(:Bobs, 'bobs', :label => 'Bobs')
  view_ref(:Summary, 'mechanics', :label => 'Should Be Hidden!')
  view_ref(:Summary, 'cities', :label => 'Cities')
  hide('mechanics')
  disable('cities')
  disable_creation('bobs')
  disable_deletion('bobs')
  html(:label => 'Link Test', :html => proc{
    haml = <<-EOS
    :sass
      .div_style
        :text-align right
        :display block
        
    %div.div_style
      %a.a_style{:href => 'https://prometheuscomputing.com/', :target=> '_blank'} Prometheus Computing (opens in new tab)
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  })
  # Note that the style is specified in two different ways here -- both should work.
  # Note also that one has :label explicitly specified and the other does not.  When unspecified, the label will be set to the text.  The label allows reordering.
  text_label('Blue Label', :label => 'blue_label', :style => 'font-size:16px; text-align:center; display:block; background-color:lightblue')
  text_label('Gold Label', :style => {'font-size' => '20px', 'text-align' => 'center', 'display' => 'block', 'background-color' => 'gold'})
  reorder('label_Gold Label', 'people', 'bobs', 'vehicles', 'users', 'repair_shops', to_beginning: true)
  reorder('blue_label', 'Link Test', to_end: true)
}

# destroy(:Details, Gui::Home, :Organizer)
#
# organizer(:Details, Home) {
#   view_ref(:Summary, 'users', :label => 'Users')
# }

#Workflow Specs
#-------------Hides workflow only attributes really only here to stop test errors-----------------------#
organizer(:Details, Automotive::Car) {
  hide('state', 'inspection_completed')
}

organizer(:Details, Automotive::Minivan) {
  hide('state', 'inspection_completed')
}

organizer(:Details, Automotive::HybridVehicle) {
  hide('state', 'inspection_completed')
}

organizer(:NewCarRepairOrderCreated, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'components', 'drivers', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'inspection_completed')
}

organizer(:StartedVehicleRepair, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model')
}

organizer(:StartedVehicleInspection, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers')
  disable('serial', 'make', 'vehicle_model', 'components')
}

organizer(:InspectedVehicle, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model', 'components')
}

organizer(:StartedEngineInspection, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model', 'components')
}
organizer(:StartedTireInspection, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model', 'components')
}

#End of Workflow Specs