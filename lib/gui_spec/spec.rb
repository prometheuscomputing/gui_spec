module GuiSpec
  class Spec
    attr_reader :views
    attr_accessor :covered_packages
    attr_accessor :global_spec_modifiers
    attr_accessor :default_view_sort

    # Resolve any pending actions such as global spec modifiers or linking view refs
    def resolve
      apply_global_spec_modifiers
      resolve_view_refs
      clear_global_spec_modifiers
    end

    def apply_global_spec_modifiers
      global_spec_modifiers.each { |gsm| views.each { |key, view| gsm.call(view) } }
    end
    
    def initialize
      @views = {}
      @global_spec_modifiers = []
    end

    def sort_views
      views.each { |key, view| sort_view(view) }
    end
    
    def sort_view(view)
      return if view.manually_sorted
      # FIXME this is really, really at odds with the fact that specGen does not produce views with randomaly sorted contents.  This should probably be deprecated.
      # If custom default sort is specified, use it instead of the one defined below.
      if default_view_sort
        @content.sort_by! &GuiSpec.spec.default_view_sort
      else
        view.default_sort
      end
    end

    def clear_global_spec_modifiers
      @global_spec_modifiers = []
    end

    def default_view_type
      Gui.option(:default_view_type)
    end
    
    def default_view_type= view_type
      Gui.option(:default_view_type, view_type)
    end

    def default_view_name
      Gui.option(:default_view_name)
    end
    
    def default_view_name= view_name
      Gui.option(:default_view_name, view_name)
    end

    def default_domain_object
      Gui.option(:default_domain_object)
    end
    
    def default_domain_object= domain_obj
      Gui.option(:default_domain_object, domain_obj)
      puts "Set :default_domain_object to #{domain_obj.inspect} and the option is now #{default_domain_object.inspect}"
    end
    
    def default_classifier
      default_domain_object.class
    end

    # TODO: Separate setter
    def default_language
      lang = Gui.option(:default_text_language)
      # reset it if it isn't a MarkupType
      if defined?(Gui_Builder_Profile::MarkupType) && !lang.is_a?(Gui_Builder_Profile::MarkupType)
        Gui.option(:default_text_language, lang)
        lang = Gui.option(:default_text_language)
      end
      lang.to_s
    end
    
    def default_language= lang
      if defined?(Gui_Builder_Profile::MarkupType) # maintain backwards compatibility...
        lang = Gui_Builder_Profile::MarkupType.where(:value => lang.to_s).first
      end
      Gui.option(:default_text_language, lang)
    end

    # Retrieves a view that should have been stored by Spec#store_view
    # Look for views with the given classifier, traveling up the inheritance tree until
    #
    # @param [Gui::Widgets::ViewRef] the ViewRed used to look up the View
    # @return [Gui::Widgets::View] the View that matches the ViewRef
    def retrieve_view view_name, classifier, view_type, ignore_failure = false
      view = nil
      # Get an array of all allowable classifiers
      # This adds the classifier to the beginning of an array of its parents (sorted by closest first)
      classifiers = [classifier] + classifier.parents(:traversal => :breadth) + classifier.interfaces + [nil]
      classifiers.each do |klass|
        view = retrieve_exact_view(view_name, klass, view_type)
        break if view
      end
      
      raise \
        "Did not find a view compatible with view name #{view_name.inspect}, view type #{view_type.inspect}, and classifier" +
        " #{classifier.inspect}" unless view || ignore_failure
      
      view
    end
    
    def retrieve_exact_view view_name, classifier, view_type
      @views[[view_name, classifier, view_type]]
    end
    
    def destroy_exact_view view_name, classifier, view_type
      @views.delete([view_name, classifier, view_type])
    end
    
    # Retrieve all views for a given classifier
    def retrieve_views_for_classifier(classifier)
      @views.select { |key| key[1] == classifier }
    end
    
    # Find all views matching the passed criteria
    def find_views(view_name = nil, classifier = nil, view_type = nil)
      v = @views.select do |key|
        (view_name.nil? || key[0] == view_name) &&
        (classifier.nil? || key[1] == classifier) &&
        (view_type.nil? || key[2] == view_type)
      end
      v.values#map { |key, view| view}
    end
    
    # Retrieve an existing view by parsing values from a given view.
    # Does not raise an error if the view can't be found (unlike retrieve_view)
    def retrieve_view_from_widget(view)
      retrieve_exact_view(view.view_name, view.data_classifier, view.view_type)
    end

    # Stores a view to be later retrieved by retrieve_view
    #
    # @param [View] the view to be stored
    # @return [Gui::Widget] the stored widget
    def store_view view
      @views[[view.view_name, view.data_classifier, view.view_type]] = view
    end
    
    # Links all the view_refs to views within this spec
    def resolve_view_refs
      # Iterate over views
      @views.each do |key, view|
        view_name, classifier, view_type = key
        
        # Take all children widgets that are view_refs & link them if they are not linked already
        # TODO: Don't use direct -- TODO explain what this comment means!  'direct' what???
        view.content.each do |widget|
          if widget.is_a?(Gui::Widgets::ViewRef) && !widget.view
            widget.derive_view(view, self)
          end
        end
      end
    end
    
    def clear_all
      clear_views
    end
    
    # Clears the views from storage
    #
    # @return [Hash] the views storage container
    def clear_views
      @views.clear
      @views
    end

    def inspect
      "GuiSpec::Spec {\n" + @views.values.map { |v| v.inspect.indent }.join("\n") + "\n}"
    end
  end
end

# For backwards compatibility, make GuiSpec::Spec available as Gui::Spec
module Gui
  Spec = GuiSpec::Spec
end
