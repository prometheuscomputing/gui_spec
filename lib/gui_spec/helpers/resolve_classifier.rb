module GuiSpec
	ResolveError = Class.new(Exception)
	ExpectedArrayContentError = Class.new(ResolveError)
	UnexpectedNilError = Class.new(ResolveError)
	
	def self.resolve_classifier(classifier, namespace = Object)
		resolved_classifier = case classifier
		when String
			classifier.split("::").inject(namespace) { |context, step| context.const_get(step) } rescue nil
		when Module
			classifier
		when Symbol
			resolve_classifier(classifier.to_s, namespace)
		end

    raise(ResolveError, "failed to resolve path #{classifier.inspect} in namespace #{namespace.inspect}") unless resolved_classifier
    resolved_classifier
	end
end