# Mixin to confer number slider functionality
module Gui
  module Widgets
    module NumberSlider
      attr_accessor :min_value, :max_value, :step_increment#, :range
      def initialize(args = {})
        super(args)
        @min_value       = args[:min_value]
        @max_value       = args[:max_value]
        @step_increment  = args[:step_increment]
        # @range           = args[:range] || -Infinity..Infinity
      end
    end
  end
end
