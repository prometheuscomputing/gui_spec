# Mixin to the ability to be rendered in a Collection Widget
module Gui
  module Widgets
    module Collectionable
      attr_accessor :collection_column
      def initialize(args = {})
        super(args)
        @collection_column = args[:collection_column]
      end
    end
  end
end
