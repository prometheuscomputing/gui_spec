module Gui
  module Widgets
    class Context < View
      attr_accessor :content
      def view_type; :Context; end
      
      def initialize(args = {})
        @content = args[:content] || [] # Will hold array of Gui::Widget
        raise "Context must have a getter!" unless args[:getter]
        @getter  = args[:getter]
        
        info = args[:parent].data_classifier.associations[args[:getter].to_sym]
        dc = Object.const_get(info[:class])
        args[:classifier] = dc
        super(args)
        @lazy_load_content = false
      end
      
      def context?
        true
      end
      
      def self.dsl_parameters
        [:getter]
      end
      
    end
  end
end
