module Gui
  module Widgets
    # For now, this becomes a Table. Eventually may allow TreeTable.
    # Includes functionality to add/delete row(s), constrained by muliplicity.
    # Includes ability edit a row.
    class Collection < View
      # A Range
      attr_accessor :multiplicity
      attr_accessor :page_size
      attr_accessor :orderable
      attr_accessor :columns
      attr_accessor :hide_types

      def view_type; :Collection; end
      
      def initialize(args = {})
        @multiplicity      = args[:multiplicity].kind_of?(Range) ? args[:multiplicity] : (0..Infinity)
        # columns not typically populated at init time...at least that is what it looks like March 2, 2021
        @hide_types        = args[:hide_types] || false
        super(args)
        @lazy_load_content = args[:lazy_load_content] || true
        @page_size = args[:page_size] || Gui.option(:default_page_size)
      end
      
      # Override view_ref method, to disallow adding view_refs to collections
      def view_ref(*args)
        raise "Putting view_refs in collections is not allowed"
      end
    end
  end
end