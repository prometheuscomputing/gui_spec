require_relative '../../ruby_extensions/time'
require 'fileutils'

module Gui
  # Represents a file upload
  module Widgets
    class File < SimpleWidget
      include Collectionable
      def data_classifier; Gui_Builder_Profile::File; end

      def initialize(args = {})
        super(args)
        @search_filter = {:field => :filename, :filter_type => :case_insensitive_like, :field_type => :complex_attribute}
      end
      
      class << self
        # While we do this in a more modular fashion in the general case (i.e. SimpleWidget), we streamline things here...because we can and because it makes things less confusing.
        def check_concurrency(domain_obj:, getter:, current_value:, widget_info:, opts:{})
          answer = {}
          if widget_info['data'] # should always be there!
            if widget_info['data']['filename']
              submitted_file = widget_info['data']['filename'] # this is a hash with keys [:filename, :type, :name, :tempfile, :head]
            elsif widget_info['data']['previous_filename'] && !widget_info['data']['previous_filename'].empty?
              submitted_file = widget_info['data'] # this is the name of a file that was stored as a tempfile
            else
              submitted_file = nil
            end
            # # This is ugly because the previous filename and mime type should probably not be stuck in with submitted value and should instead be in with initial value.
            # if widget_info['data']['previous_filename'] && !widget_info['data']['previous_filename'].empty?
            #   initial_value = get_access_time(widget_info['data']['previous_filename'])
            # end
          end
          initial_value = widget_info['initial_data']['last_update']
          
          # puts "CHECK CONCURRENCY for #{self}"
          # puts "****Submitted:";pp widget_info['data']; puts "****Initial: #{widget_info['initial_data']}";
          
          begin
            concurrency_check = check_attribute_concurrency(domain_obj:domain_obj, getter:getter, current_value:current_value, initial_value:initial_value, submitted_value:submitted_file, opts:opts)
            if concurrency_check[:problem]
              # Is this the right key?  Do we need it at all?  Can it just be :value?
              concurrent_change = {:filename => find_value_change(current_value, initial_value, submitted_file, opts)}
              answer[:concurrency_issues] ||={}
              answer[:concurrency_issues][widget_info['getter']] = concurrent_change
              answer[:concurrency_issues][widget_info['getter']][:label]  = widget_info['label']
              answer[:concurrency_issues][widget_info['getter']][:widget] = widget_info['widget']
              answer[:concurrency_issues][widget_info['getter']][:hover]  = widget_info['hover']
              # Will always be present if present on the widget template page.
              answer[:concurrency_issues][widget_info['getter']][:delete] = widget_info['delete'] == true
            end
            # No way to use concurrency_messages at this time -MF Feb. 10, 2016
            if concurrency_check[:message]
              answer[:concurrency_messages] ||= []
              answer[:concurrency_messages] << concurrency_check[:message]
            end
            if concurrency_check[:warning]
              answer[:concurrency_warnings] ||= []
              answer[:concurrency_warnings] << concurrency_check[:warning]
            end
          rescue Gui::WidgetValueError => e
            answer[:errors] ||= []
            answer[:errors] << e.message
          end
          answer
        end

        # value here is passed from the form by Ramaze handling of multipart requests as a Hash
        # Alternatively, the file can be specified via an Array of values, if we are in a 'continue_edit' state
        def cast_value_for_setting(value = {}, opts = {})
          make_file_from_value(value, opts)
          # NOTE: This used to be an instance method. It never was possible to parse a string that was multiple because this was being called on a widget that was created by Gui::Widgets#get_widget, which could not possibly have returned any widget with multiple set to true!
          # if self.multiple # I doubt this actually works -SD # Concur -BD # it couldn't even have ever been called -MF
          #   value.map { |v| make_file_from_value(v) }
          # else
          #   make_file_from_value(value)
          # end
        end

        def empty_data?(data)
          data.nil? || data.empty? || !has_data?(data)
        end
        
        def has_data?(hash)
          return true if hash.dig('filename', :filename)
          hash['previous_filename'] && !hash['previous_filename'].empty?
        end

        private
        def check_attribute_concurrency(domain_obj:, getter:, current_value:, initial_value:, submitted_value:, opts:{})
          no_problem = {}
          return no_problem if domain_obj.new? # it is impossible for two users to concurrently be working on the same new object so there is no need to check if the value has changed.
          # unless initial_value.nil? # this should never happen because we check before calling the method and already returned if this is a new object.
          #   puts "Warning from #{self.class}#check_attribute_concurrency: no last_update time was submitted for '#{domain_obj.class}##{getter}'"
          #   return no_problem
          # end
          # puts "CURRENT FILE UPDATED_AT: #{current_file.binary_data[:updated_at].to_s_usec}"
          formatted_current_value = format_value(current_value, opts.merge({:formatting_context => :concurrency_check, :value_type => :current}))
          submitted_value = format_value(submitted_value, opts.merge({:formatting_context => :concurrency_check, :value_type => :submitted}))
          initial_value = format_value(initial_value,   opts.merge({:formatting_context => :concurrency_check, :value_type => :initial}))
          decision = decide_if_value_has_changed(initial_value, formatted_current_value, submitted_value, opts)
          decision = parse_concurrency_warnings(decision, initial_value, submitted_value, current_value, opts)
          decision
        end # def check_attribute_concurrency

        def get_access_time(filename, directory = Gui.option(:tempfile_dir))
          file = ::File.join(directory, filename)
          if ::File.exist?(file)
            ::File.stat(file).atime.to_s_usec
          else
            nil
          end
        end      

        # We compare files by checking to see when their associated BinaryData objects were last updated in the database
        def format_value(value, opts = {})
          case opts[:formatting_context]
          when :concurrency_check
            case opts[:value_type]
            when :initial
              value
            when :current
              file = opts[:domain_obj].send(opts[:getter])
              bd   = file.binary_data if file
              if bd
                compare_time = (bd[:updated_at] || bd[:created_at])
                if compare_time
                  compare_time.to_s_usec
                else
                  ""
                end
              end
              # ((bd[:updated_at] || bd[:created_at]) || "").to_s_usec if bd
            when :submitted
              # puts "!!!!!!!!!!!! Formatting submitted value: #{value.inspect}, for concurrency check with key: #{opts[:key]}"
              # This basically ensures that if a file has been newly submitted that it will have a different value than either initial or current
              if value.is_a?(Hash)
                if value[:tempfile]
                  value[:tempfile].atime.to_s_usec
                elsif value['previous_filename']
                  # We are really, really hoping that the only other thing we get here the filename of a temporarily stored file.
                  get_access_time(value['previous_filename'])
                end
              else
                value # which we expect to be nil if it isn't a hash
              end
            else
              raise "Unknown case for value formatting for this File"
            end
          when :find_value
            case opts[:value_type]
            when :current
              opts[:domain_obj].send(opts[:getter])
            when :submitted
              value
              # if value.is_a?(Hash)
              #   value[:filename]
              # else
              #   value
              # end
            else
              raise "I don't know how to find the value for the #{opts[:value_type]} value in this context."
            end
          end
        end

        # Files are different because they really represent an underlying association (the BinaryData).  For this reason we have to have to take submitted value into account.  If the user has submitted a new file and there is an existing file then they will get to decide whether to keep their file or the concurrently uploaded one.  If the user has not tried to upload a new file but the current and initial values do not agree then someone has concurrently uploaded a new file and we should just leave it there. 
        def decide_if_value_has_changed(initial_value, current_value, submitted_value, opts)
          initial_value   = nil if initial_value   && initial_value.empty?
          submitted_value = nil if submitted_value && submitted_value.empty?
          current_value   = nil if current_value   && current_value.empty?
          # none of these will be an empty String, so we don't have to worry about that.  These passed in values have been formatted.
          # puts "Deciding if file has changed:"
          # puts "    initial_value:   #{initial_value.inspect}"
          # puts "    current_value:   #{current_value.inspect}"
          # puts "    submitted_value: #{submitted_value.inspect}"
          case
          when current_value == initial_value
            answer = {}
          when current_value  && submitted_value
            answer = {:problem => true}
          when current_value  && !submitted_value &&  initial_value
            # concurrent replacement of file with a new file
            answer = {:warning => :file_replaced}
          when current_value  && !submitted_value && !initial_value
            # concurrent addition of file
            answer = {:warning => :file_added}
          when !current_value && initial_value && submitted_value
            # concurrent deletion of file while I am trying to replace the file myself
            answer = {:warning => :file_delete_replaced}
          when !current_value && initial_value && !submitted_value
            # concurrent deletion of file
            answer = {:warning => :file_deleted}
          end
          # puts "    #{answer[:problem] ? "PROBLEM: #{answer.inspect}" : "no problem: #{answer.inspect}"}"
          answer
        end
      
        # TODO: I wanted to send some of these as warnings but for that to be useful there needed to be a good way to display them on the page.  As it is, the message was just getting added to the 'updated fields' list.  More stuff would need to be built out for it to be displayed nicely on the page. -MF
        def parse_concurrency_warnings(decision, initial_value, submitted_value, raw_current_value, opts)
          if w = decision[:warning]
            message = nil
            warning = nil
            label   = opts[:widget_info]['label']
            case w
            when :file_deleted
              warning = "The file for '#{label}' was concurrently deleted in another open tab or by another user."
            when :file_delete_replaced
              warning = "The file for '#{label}' was concurrently deleted by another user or in another tab but you have replaced it."
            when :file_added
              warning = "The file for '#{label}' was concurrently added by another user or in another open tab."
            when :file_replaced
              warning = "The file for '#{label}' was concurrently replaced in another open tab or by another user."
            else
              raise "Unknown warning type for File concurrency!"
            end
            decision[:warning] = warning
            decision[:message] = message
          end
          decision
        end

        def make_file_from_value(value = {}, opts = {})
          # puts "make_file_from_value: #{value.inspect}"
          # opts currently unused but it might be handy someday...
          # puts "make_file_from_value: #{value.inspect}"
          # If it's a hash, then it has the location of tempfile in the parameter
          if value['filename'].is_a?(Hash)
            # File is loaded from parameter with normal function
            # Go one level deeper in hash
            value     = value['filename']#.with_indifferent_access
            filename  = value[:filename]
            data      = ::File.binread(value[:tempfile])
            mime_type = value[:type]
          elsif value["previous_filename"] && !value["previous_filename"].empty?
            # This case is when you save a file from a continued edit
            # In other words, when the parameters are stored in flash, then reloaded and saved.
            filename  = value["previous_filename"]
            tempfile  = ::File.join(Gui.option(:tempfile_dir), filename)
            data      = ::File.binread(tempfile)
            mime_type = value["previous_mime_type"]
          else
            return nil
          end
          Gui_Builder_Profile::File.instantiate(:filename => filename, :data => data, :mime_type => mime_type)
        end

      end # end class methods
    end
  end
end
