module Gui
  module Widgets
    # Represents a single line of unstyled text
    class String < SimpleWidget
      include Collectionable
      
      # For the moment, let's ignore validation by the regexp
      attr_accessor :regexp
      attr_accessor :placeholder_text
      attr_accessor :min_value
      attr_accessor :max_value
      attr_accessor :save_form_field_draft
      def data_classifier; ::String; end

      def initialize(args = {})
        super(args)
        @regexp           = args[:regexp] || /.*/
        @placeholder_text = args[:placeholder_text]
        @min_value        = args[:min_value]
        @max_value        = args[:max_value]
        @save_form_field_draft = args[:save_form_field_draft]
      end
      
      class << self
        def cast_value_for_setting(value = {}, opts = {})
          value['value'].to_s.strip
        end
      end
    end
  end
end
