module Gui
  module Widgets
    class TextLabel < SimpleWidget
      attr_accessor :text
      attr_accessor :font
      attr_accessor :font_size
      attr_accessor :data_classifier # Not needed but required for some reason

      def initialize(args = {})
        @label           = args[:label]
        super(args)
        @data_classifier = NilClass
        @text            = args[:text]
        @font            = args[:font]
        @font_size       = args[:font_size]
      end
      
      def id_string
        "label_#{text}"
      end
      
      def inspect
        "#{self.class.name.split('::').last}: text=#{@text.inspect}#{'(hidden)' if @hidden}"
      end
      
      def self.dsl_parameters
        [:text]
      end
      
      def self.dsl_aliases
        [:label]
      end
    end
  end
end