require_relative '../../ruby_extensions/string'
require_relative 'choice'

module Gui
  module Widgets
    class Boolean < Choice

      attr_accessor :save_form_field_draft
      
      def initialize(args = {})
        # TODO add ability to set default value and checkbox display
        super(args)
        @choices = args[:choices] || ['True', 'False']
        @search_filter = :boolean
        @save_form_field_draft = args[:save_form_field_draft]
      end
      
      # A word about @choices -- This was all well and good that it could be something other than True or False but there was never any way to cast true or false to anything special because we were calling #cast_value_for_setting not on the original widget but on one that was newly created during parse_attribute_changes.  Of course we aren't doing that now.  The point is that while it may have appeared that we could use values other than True or False, we really never could.
      
      # Class methods
      class << self
        def cast_value_for_setting(value = {}, opts = {})
          value['value'].to_s.to_boolean
        end
        
        private
        def format_value(value, opts = {})
          choices = ['True', 'False']
          case opts[:formatting_context]
          when :concurrency_check
            case value
            when choices[0]
              true
            when choices[0].downcase
              true
            when choices[1]
              false
            when choices[1].downcase
              false
            else
              value
            end
          when :find_value
            case value
            when true
              choices[0]
            when false
              choices[1]
            end
          else
            raise "Unknown context for formatting of this boolean value: #{value.inspect}"
          end
        end
        
      end
    end
  end
end
