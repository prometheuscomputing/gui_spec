# TODO this seems like a crappy widget.  Investigate its usage and maybe make things better.
module Gui
  module Widgets
    class Link < SimpleWidget
      # A Symbol used to look up a View
      attr_accessor :view_name
      attr_accessor :data_classifier

      def initialize(args = {})
        super(args)
        @data_classifier = NilClass
        @view_name       = args[:view_name]
      end
      
      def id_string
        "link_#{view_name}"
      end
    end
  end
end