module Gui
  module Widgets
    class HTML < SimpleWidget
      attr_accessor :html

      def initialize(args = {})
        @label           = args[:label]
        super(args)
        @data_classifier = Object
        @html            = args[:html] || proc { |getter_return| getter_return }
      end
      
      # getter_return param is the result of calling @getter on the parent_obj, if @getter was specified
      def literal_html(getter_return = nil)
        case @html
        when String
          @html
        when Proc
          @html.call(getter_return)
        end
      end
      
      def id_string
        "html_#{html.object_id}"
      end
      
      def self.dsl_parameters
        []
      end
    end
  end
end