module Gui
  module Widgets
    # Represents a single line of unstyled text
    class Button < SimpleWidget
      attr_accessor :data_classifier # Not needed but required for some reason
      attr_accessor :action
      attr_accessor :save_page
      attr_accessor :button_text
      attr_accessor :display_result # :none, :popup, :web_page, :file
      attr_accessor :image_actions # An array of method callbacks that return image streams (to be used in popup)
      attr_accessor :validation_action
      def data_classifier; ::Object; end

      def initialize(args = {})
        super(args)
        @data_classifier   = Object # Should remove this but data_classifier is required
        @action            = args[:action]
        @getter            = args[:action] # Set getter even though it is not directly used, so that comparison methods can find it.
        @save_page         = args[:save_page]
        @button_text       = args[:button_text]
        @display_result    = args[:display_result] || :none
        @image_actions     = args[:image_actions] || []
        @validation_action = args[:validation_action]
      end
      
      def self.dsl_parameters
        [:action]
      end
    end
  end
end
