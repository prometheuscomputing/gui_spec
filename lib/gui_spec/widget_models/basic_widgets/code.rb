module Gui
  module Widgets
    DEFAULT_CODE_LANGUAGES = ['XML', 'Javascript', 'JSON', 'HTML', 'Ruby', 'CSS', 'SQL']
  
    class Code < AbstractText
      def data_classifier; ::Gui_Builder_Profile::Code; end

      def initialize(args = {})
        super(args)
        @language                = args[:language]
        @languages               = args[:languages] || DEFAULT_CODE_LANGUAGES
        @show_language_selection = Gui.option(:show_code_languages) if @show_language_selection.nil?
        @always_collapsed = Gui.option(:code_always_collapsed) || false if @always_collapsed.nil?
      end
      
      class << self
        
        # FIXME see text.rb for ideas
        def cast_value_for_setting(value = {}, opts = {})
          # FIXME why not respond to #multiple????
          existing_domain_obj = opts[:domain_obj].send(opts[:getter]) if opts[:domain_obj]
          # raise "Multiple codes are not currently supported" if self.multiple

          new_content  = !(existing_domain_obj.nil? && value['content'] == '')
          new_language = value['language']
          
          return nil if !new_content && !new_language
          
          if defined?(Gui_Builder_Profile::LanguageType)
            language = Gui_Builder_Profile::LanguageType.find(:value => value['language'])
          else
            language = value['language']
          end
          
          if existing_domain_obj
            language ||= existing_domain_obj.language
          end
          
          code_object = Gui_Builder_Profile::Code.new(:content => value['content'])
          code_object.language = language
          code_object
        end

        # # FIXME Implement!
        def check_concurrency(domain_obj:, getter:, current_value:, widget_info:, opts:{})
          # super
          #TODO turned this off for now since it doesn't work at all -TB
          {}
        end
        
        private
        def get_langauge_value(string_or_enumeration_literal)
          return default_language_string unless string_or_enumeration_literal
          if string_or_enumeration_literal.is_a?(Gui_Builder_Profile::LanguageType)
            string_or_enumeration_literal.value
          else # assuming it is a String
            string_or_enumeration_literal
          end
        end
        
        def default_language
          # FIXME!
          DEFAULT_CODE_LANGUAGES.first
        end
      end
    end
  end
end
