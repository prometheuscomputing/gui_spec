module Gui
  module Widgets
    class Integer < Numeric
      include Collectionable
      def data_classifier; ::Integer; end

      def initialize(args = {})
        super(args)
      end
      
      class << self
        protected
        def _convert_to_type(value)
          Integer(value.to_s)
        end
      end
    end
  end
end
