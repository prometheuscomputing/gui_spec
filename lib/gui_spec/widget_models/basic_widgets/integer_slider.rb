require_relative 'integer'

module Gui
  module Widgets
    class IntegerSlider < Integer
      include NumberSlider
    end
  end
end
