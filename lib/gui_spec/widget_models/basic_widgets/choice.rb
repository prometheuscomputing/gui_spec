module Gui
  module Widgets
    class Choice < SimpleWidget
      include Collectionable
      # Can be:
      # * an Array of literals
      # * A string that's executed in the context of the instance to get a list of valid values
      attr_accessor :choices
      attr_accessor :literals_getter
      attr_accessor :save_form_field_draft
      # def data_classifier; ::Object; end
      
      # Override normal default message, since it doesn't sound right for choice widgets
      def default_display
        @default_display || '[Not Set]'
      end

      def initialize(args = {})
        super(args)
        @choices         = args[:choices]
        @literals_getter = "#{args[:getter]}_literals".to_sym
        @save_form_field_draft = args[:save_form_field_draft]
      end
      
      # FIXME so it returns the attribute type that this widget maps to
      # I don't really understand.  At this point, enums are always Strings so this should've been a no-brainer, right?
      # Shouldn't we do this at init time and save to instance var?
      def data_classifier
        prnt = parent
        if prnt
          enum_type_getter = @literals_getter.to_s.sub('_literals', '_type').to_sym
          return prnt.send(enum_type_getter) if prnt.respond_to?(enum_type_getter)
        end
        ::Object
      end
      
      class << self
        def cast_value_for_setting(value = {}, opts = {})
          if self.multiple # If multiple, then value should be an array in hash
            # value.values.flatten.map { |val| val.to_s }
            # FIXME this raise is in here as a test
            value['value'].map { |val| val.to_s }
          else # Should value should be single value in hash
            value['value'].to_s
          end
        end
        
        private
        def format_value(value, opts = {})
          # FIXME can we get away with just returning value? that means it always has to be an instance of the enumeration class
          return value
          if value.is_a? String
            enum_class = domain_obj.class.properties[getter.to_sym][:class].to_const
            enum_class.where(:value => value).first
          else
            if value.is_a? enum_class # it better be an instance of enum_class...
              value
            else
              # TODO get mad and raise?
              value
            end
          end
        end
      end # class methods    
    end
  end
end