module Gui
  module Widgets
    # A date picker
    class Timestamp < SimpleWidget
      include Collectionable      
      def data_classifier; ::Time; end

      def initialize(args = {})
        super(args)
        @search_filter = :timestamp
      end

      def self.pretty_time(t)
        t.is_a?(::Time) ? t.strftime("%B %-d, %Y %l:%M:%S%P").gsub(/\s\s+/, ' ') : t 
      end

      class << self
        # assumes an input of "MM/DD/YYYY" and converts it to DD/MM/YYYY
        def cast_value_for_setting(value = {}, opts = {})
          # TODO: Handle multiple
          # Check for empty value since ::Time.parse('') returns today's date instead of raising an error or returning '' or nil
          if value['value'].nil? || value['value'].empty?
            nil
          else
            begin
              ::Time.parse(value.to_s)
            rescue ArgumentError
              raise WidgetValueError, "Could not save the '#{opts[:label]}' field. Please enter a #{type_name}."
            end
          end
        end

        private
        def format_value(value, opts = {})
          self.pretty_time(value)
        end
      end # end class methods
    end
  end
end