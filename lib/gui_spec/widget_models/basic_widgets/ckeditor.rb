require_relative 'rich_text'

module Gui
  module Widgets
    class CKEditor < RichText
      def initialize(args = {})
        super(args)
        @language = 'HTML'
      end
      
      def self.dsl_aliases
        [:ckeditor]
      end
    end
  end
end