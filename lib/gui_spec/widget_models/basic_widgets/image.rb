require_relative 'file'

module Gui
  module Widgets
    class Image < Gui::Widgets::File # Fully qualified name in order to disambiguate from Ruby core library ::File
      # There are two source types: :url and :getter
      # :url images may be links outside the site and so are not able to be saved/stored
      # :getter images may be saved unless :disabled is also specified (e.g. generated images)
      attr_accessor :source_type
      attr_accessor :url
      attr_accessor :height
      attr_accessor :width

      def initialize(args = {})
        super(args)
        # Allow soruce to be specified as either a URL or a getter
        if args[:source] 
          if args[:source].index('/') # probably a URL
            args[:url] = args[:source]
          else
            args[:getter] = args[:source]
          end
        end

        if args[:url]
          @source_type = :url
          @url         = args[:url]
        elsif args[:getter]
          @source_type = :getter
        end
        @height        = args[:height]
        @width         = args[:width]
      end
      
      def self.dsl_parameters
        [:source]
      end
    end
  end
end