module Gui
  module Widgets
    DEFAULT_TEXT_LANGUAGES = ['Markdown', 'LaTeX', 'Plain', 'HTML', 'Textile', 'Kramdown']
    # DEFAULT_SHOW_TEXT_LANGUAGE_SELECTION = false
  
    # Represents multiple lines of (possibly) styled text.
    class RichText < AbstractText
      def data_classifier; ::Gui_Builder_Profile::RichText; end

      def initialize(args = {})
        super(args)
        @language  = args[:language]  || Gui.option(:default_text_language)
        @languages = args[:languages] || Gui.option(:text_languages) || DEFAULT_TEXT_LANGUAGES
        @show_language_selection = Gui.option(:show_text_languages) if @show_language_selection.nil?
        @always_collapsed = Gui.option(:text_always_collapsed) || false if @always_collapsed.nil?
        # Note that @expanded and @always_collapsed can be conflicting.  In html_gui_builder's text.haml it is the value of @expanded that takes precedence over @always_collapsed.  This is becaused the former is set on a per widget basis and is thus more granular / specific than the latter which is determined by a global setting.
        @expanded = args[:expanded] || false # Override default to be false
      end
      
      class << self
        def dsl_aliases
          [:richtext, :text]
        end
        
        def canonical_column_name
          :text
        end
        
        def default_language
          # i.e Gui.loaded_spec.default_TEXT_language -- method was not named with enough specificity
          # The language is either the literal language, or a string that when
          # "resolve_in" is called will yield the language
          Gui.loaded_spec.default_language
        end
                
        # While we do this in a more modular fashion in the general case (i.e. SimpleWidget), we streamline things here...because we can and because it makes things less confusing.  It is hard to use the general case with complex attributes.  (Complex attributes are kind of yucky.)
        def check_concurrency(domain_obj:, getter:, current_value:, widget_info:, opts:{})
          answer = {}
          if widget_info['data'] # should always be there!
            if widget_info['data']['content']
              submitted_text = widget_info['data']['content']
            else
              submitted_text = nil
            end
            if widget_info['data']['content']
              submitted_language = get_langauge_value(widget_info['data']['markup_language'])
            else
              submitted_language = default_language_string
            end
          end
          if widget_info['initial_data'] # should always be there!
            if widget_info['initial_data']['content']
              initial_text = safe_string(widget_info['initial_data']['content'])
            else
              initial_text = nil
            end
            if widget_info['initial_data']['content']
              initial_language = get_langauge_value(widget_info['initial_data']['markup_language'])
            else
              initial_language = default_language_string
            end
          end
          
          begin
            # concurrency_check = {:problem => !(current_richtext.eql?(initial_richtext))} # Why is this failing to tell us that RichTexts with the same markup language and content are actually the same? TODO figure out
            if current_value # There *should* always be a current RichText (unless this is a new domain object, in which case we exit before getting here)
              current_text     = safe_string(current_value.content)
              current_language = get_langauge_value(current_value.markup_language)
            else
              current_text     = ""
              current_language = default_language_string
            end
            content_differs         = current_text     != initial_text
            content_differs = false if (current_text.nil? && initial_text == "") || (current_text == "" && initial_text.nil?)
            markup_language_differs = current_language != initial_language
            if content_differs || markup_language_differs
              # if content_differs
              #   puts "current_text:"
              #   puts current_text.inspect
              #   puts "initial_text:"
              #   puts initial_text.inspect
              # end
              # if markup_language_differs
              #   puts "current_language: #{current_language.inspect}; initial_language: #{initial_language.inspect}"
              # end
              new_richtext = Gui_Builder_Profile::RichText.new(:content => submitted_text)
              new_richtext.markup_language = submitted_language
              initial_richtext = Gui_Builder_Profile::RichText.new(:content => initial_text) if initial_text && !initial_text.empty?
              initial_richtext.markup_language = initial_language if initial_richtext
              # Is :content the right key?  Can it just be :value? It really should just be :value but that means more refactoring
              concurrent_change = {:content => {:initial_value => initial_richtext, :current_value => current_value, :new_value => new_richtext}}
              answer[:concurrency_issues] ||={}
              answer[:concurrency_issues][widget_info['getter']] = concurrent_change
              answer[:concurrency_issues][widget_info['getter']][:label]  = widget_info['label']
              answer[:concurrency_issues][widget_info['getter']][:widget] = widget_info['widget']
              answer[:concurrency_issues][widget_info['getter']][:hover]  = widget_info['hover']
              # Will always be present if present on the widget template page.
              answer[:concurrency_issues][widget_info['getter']][:delete] = widget_info['delete'] == true
            end
            # # No way to use concurrency_messages at this time -MF Feb. 10, 2016
            # if concurrency_check[:message]
            #   answer[:concurrency_messages] ||= []
            #   answer[:concurrency_messages] << concurrency_check[:message]
            # end
            # if concurrency_check[:warning]
            #   answer[:concurrency_warnings] ||= []
            #   answer[:concurrency_warnings] << concurrency_check[:warning]
            # end
          rescue Gui::WidgetValueError => e
            answer[:errors] ||= []
            answer[:errors] << e.message
          end
          answer
        end
        
        def cast_value_for_setting(value = {}, opts = {})
          existing_domain_obj = opts[:domain_obj].send(opts[:getter]) if opts[:domain_obj]
          # if there is an existing domain object then we assume it has content.  If we are also passing in an empty string for the content then we are deleting all of the existing content.
          new_content  = !(existing_domain_obj.nil? && value['content'] == '')
          new_language = !(value['markup_language'] == default_language)
          new_images   = !(value['uploaded_images'].nil? || value['uploaded_images'].empty?)

          # Return nil if the new object to create has no data, images, or new language in it. No reason to create new object with empty values.
          return nil if !new_content && !new_language && !new_images

          # TODO: Handle multiple case
          if defined?(Gui_Builder_Profile::MarkupType)
            markup_language = Gui_Builder_Profile::MarkupType.where(:value => value['markup_language']).first
          else
            markup_language = value['markup_language']
          end
          # Keep the markup language if a new one wasn't passed. If no language existed to begin with, use default.
          markup_language ||= existing_domain_obj ? existing_domain_obj.markup_language : default_language

          content  = value['content'].strip
          richtext = Gui_Builder_Profile::RichText.new(:content => content)
          richtext.markup_language = markup_language
          # Ensure that any deleted images are not added. This would cause a basis_has_changed error.
          # FIXME this is not OK.  Can not use ChangeTracker!!
          existing_images = existing_domain_obj.images.select { |i| !i.ct_identity.refresh.deleted } if existing_domain_obj
          richtext.images = existing_images if existing_images && existing_images.any?

          if value['uploaded_images'] && !value['uploaded_images'].empty?
            value['uploaded_images'].each do |image_info|
              rti = Gui_Builder_Profile::RichTextImage.create
              rti.image = make_file_from_value(image_info)
              richtext.add_image(rti)
            end
          end
          richtext
        end
        
        private
        def get_langauge_value(string_or_enumeration_literal)
          return default_language_string unless string_or_enumeration_literal
          if string_or_enumeration_literal.is_a?(Gui_Builder_Profile::MarkupType)
            string_or_enumeration_literal.value
          else # assuming it is a String
            string_or_enumeration_literal
          end
        end
        
        def safe_string(value)
          return value unless value.is_a?(::String)
          Gui::Utils.remove_carriage_returns(Gui::Utils.trim_leading_and_trailing_newlines(value.force_encoding('utf-8')))
        end  

        def make_file_from_value(value)
          filename  = value[:filename]
          mime_type = value[:type]
          tempfile  = value[:tempfile]
          data      = ::File.binread(tempfile)
          Gui_Builder_Profile::File.instantiate(:filename => filename, :data => data, :mime_type => mime_type)
        end
      
      end
    end
    Text = RichText
  end
end
