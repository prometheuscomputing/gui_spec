module Gui
  module Widgets
    class Float < Numeric
      include Collectionable

      def data_classifier; ::Float; end
      
      def initialize(args = {})
        super(args)
      end
      
      class << self
        # def dsl_aliases
          # NOTE these are handled in manually_defined_widget_dsl.rb so we can include a deprecation warning.
          # [:number, :fixedpoint]
        # end
        
        def canonical_column_name
          :fixedpoint
        end
        
        protected
        def _convert_to_type(value)
          Float(value.to_s)
        end
      end
    end
  end
end
