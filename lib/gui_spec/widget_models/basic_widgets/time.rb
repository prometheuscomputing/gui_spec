require_relative 'timestamp'

module Gui
  module Widgets
    # A time picker
    class Time < Timestamp

      def initialize(args = {})
        super(args)
        @search_filter = :time
      end
      
      def self.pretty_time(t)
        t.is_a?(::Time) ? t.strftime("%l:%M:%S%P") : t
      end
    end
  end
end