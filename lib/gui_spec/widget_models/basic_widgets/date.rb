require_relative 'timestamp'

module Gui
  module Widgets
    # A time picker
    class Date < Timestamp

      def initialize(args = {})
        super(args)
        @search_filter = :date
      end
      
      def self.pretty_time(t)
        t.is_a?(::Time) ? t.strftime("%B %-d, %Y") : t
      end
      
    end
  end
end