module Gui
  module Widgets
    class ViewRef < SimpleWidget
      # A Symbol used to look up a View
      attr_accessor :view_name
      attr_accessor :data_classifier
      attr_accessor :multiplicity
      attr_accessor :orderable
      attr_accessor :composer
      attr_accessor :composition
      # Overrides for object_view_name and object_view_type specific to this view_ref
      attr_accessor :object_view_name
      attr_accessor :object_view_type
      
     # TODO: Make #derive_view & #view better linked
      attr_accessor :view
      
      def initialize(args = {})
        super(args)
        assoc_info         = @parent.data_classifier.associations[getter.to_sym] if @parent
        # TODO Suggestion:  @data_classifier = args[:data_classifier] || assoc_info[:class].to_const if assoc_info
        @data_classifier   = args[:data_classifier] || args[:classifier] || assoc_info[:class].to_const if assoc_info
        # @data_classifier   = assoc_info[:class].to_const if assoc_info
        opp_assoc_info     = data_classifier.associations[assoc_info[:opp_getter]] unless !assoc_info || assoc_info[:associates]
        @view_name         = args[:view_name]
        @multiplicity      = args[:multiplicity]
        @orderable         = args[:orderable] || false
        @expanded          = args[:expanded] || false # Override default to be false
        @lazy_load_content = args[:lazy_load_content]
        @composition       = assoc_info[:composition] if assoc_info
        @composer          = opp_assoc_info[:composition] if opp_assoc_info
        @object_view_name  = args[:object_view_name]
        @object_view_type  = args[:object_view_type]
        @item_label        = args[:item_label]
        @page_size         = args[:page_size]
      end
      
      def display_type;  view.display_type;  end
      def view_type;     view.view_type;     end
      def hide_header;   view.hide_header;   end
      def table_headers; view.table_headers; end
      
      def classes; @classes | view.classes;  end
      
      # TODO: find some other way to handle this. I don't want to mirror every view method here.
      # def list_style_type; view.list_style_type; end
      
      def content
        view.content
      end
      
      def context?
        view.context?
      end

      # If the spec specifies a specific page size for this view_ref then it will take precedence over the size set for the collection
      def page_size
        @page_size || view.page_size
      end
      
      def item_label
        @item_label || view.item_label
      end
      
      # Use local override, or else view name and type defined in view
      def object_view_name
        @object_view_name || view.object_view_name
      end
      def object_view_type
        @object_view_type || view.object_view_type
      end
      
      def data_classifier
        @data_classifier || view.data_classifier
      end
            
      def lazy_load_content
        @lazy_load_content || view.lazy_load_content
      end
      
      def setter
        if @setter
          "self.#{@setter}"
        else
          @view.is_a?(Collection) ? "self.#{@getter}_add" : "self.#{@getter}="
        end
      end
      
      def getter
        @getter || (view.respond_to?(:getter) ? view.getter : nil)
      end
      
      # TODO: Simplify this now that view_refs aren't used for the edit link in collections and should always have a getter.
      def derive_view(parent_view, spec = Gui.loaded_spec)
        return if self.view
        
        if parent_view.nil?
          raise "Must have a parent to derive the view."
        end
        
        if parent_view.data_classifier.nil?
          raise "Parent must have a data classifier to derive view"
        end
        
        if parent_view.data_classifier == Object || parent_view.data_classifier == Array
          raise "Parent data classifier must be more specific than Object or Array"
        end

        # Split the getter on "."
        getters = self.getter.split(".")
        
        association_info = {}
        parent_class = parent_view.data_classifier
        # Follow the chain of getters (Only executes once if it is not a chained getter)
        getters.each do |getter|
          association_info = parent_class.associations[getter.to_sym]
          unless association_info
            message = "Unable to find association: #{getter.inspect} (resulting from getter chain #{getters.inspect} applied to class #{parent_class}).  Available association getters are: #{parent_class.associations.keys.inspect}"
            raise message
          end
          parent_class = GuiSpec.resolve_classifier(association_info[:class])
        end
        
        case association_info[:type].to_s
        when /_to_many/
          view_type = :Collection
        when /_to_one/, /associates/
          view_type = :Organizer
        else
          raise "Invalid association type '#{view_type}' in ViewRef#derive_view\n#{pretty_inspect}"
        end

        # Update the data classifier of the view_ref now that we know what it should be
        self.data_classifier = parent_class
        self.view = spec.retrieve_view(self.view_name, parent_class, view_type)
      end
      
      # If you need to change the rendering options based on information held by the ViewRef or its view then do it here.
      def process_rendering_options(options)
        return options unless view.is_a?(Gui::Widgets::Collection)
          
        # because this value came from the page as a hash of strings and we had to call to_i on it then it is going to be zero even if there was no value on the page.  "".to_i is 0.  We are setting it back to nil if it is 0 so that the rest of the logic works.
        options[:page_size] = nil if options[:page_size].to_i < 1
        options[:page_size] = options[:page_size] || page_size
        options[:page_size] = Gui.option(:min_page_size) if options[:page_size] < Gui.option(:min_page_size)
        options[:page_size] = Gui.option(:max_page_size) if options[:page_size] > Gui.option(:max_page_size)          
        
        options[:page_index] ||= 0
        # Computes max page size. Decrement max_page_index to make zero based
        if options[:collection_size] && options[:collection_size] > 0
          max_page_index = (options[:collection_size] / options[:page_size].to_f).ceil - 1
        end
        # If collection size is 0 or nil, or if page index is less than zero, reset the page index.
        if options[:page_index] < 0 || options[:collection_size].nil? || options[:collection_size] == 0
          options[:page_index] = 0
        elsif options[:collection_size] > 0
          max_page_index = (options[:collection_size] / options[:page_size].to_f).ceil - 1
          # Set to max index if the page exceeds the max index
          options[:page_index] = max_page_index if options[:page_index] > max_page_index
        end

        options[:limit]   = options[:page_size]
        options[:offset]  = options[:page_size] * options[:page_index]
        Gui.use_options([:page_size, :page_index, :limit, :offset], :set)
        Gui.use_options([:collection_size])
        options
      end
      
      def inspect
        super + if self.view
          "\n" + view.inspect.indent
        else
          '(unlinked!)'
        end
      end
      
      def self.dsl_parameters
        [:view_name, :getter, :classifier]
      end
    end
  end
end
