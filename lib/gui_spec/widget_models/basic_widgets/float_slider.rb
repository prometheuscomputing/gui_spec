require_relative 'float'
module Gui
  module Widgets
    class FloatSlider < Float
      include NumberSlider
    end
  end
end
