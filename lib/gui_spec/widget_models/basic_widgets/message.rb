module Gui
  module Widgets
    # This is intended for displaying derived attributes which are non-editable.  For example, one might want to display a summary of contextual information about a domain_obj.  This widget should not work unless the getter is added to the relevant class with #derived_attribute
    class Message < SimpleWidget
      attr_accessor :font
      attr_accessor :font_size
      def data_classifier; ::String; end # This refers more to what is in the widget and not what the type of the object being returned by the getter is

      def initialize(args = {})
        super(args)
        @font      = args[:font]
        @font_size = args[:font_size]
      end
    end
  end
end