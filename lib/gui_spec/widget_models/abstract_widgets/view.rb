require_relative 'widget'

module Gui
  module Widgets
    class View < Widget
      # A Symbol that Ref uses (together with the current_object) to find a View.
      attr_accessor :view_name
      attr_accessor :num_action_buttons
      attr_accessor :content
      attr_accessor :manually_sorted # Whether or not #sort or #sort_by has been called by the user
      # object_view_name and object_view_type define what views should be used to render an individual item in more detail (e.g. create/edit links).

      attr_accessor :lazy_load_content
      attr_accessor :hide_header

      # A boolean value of whether the global spec modifier has been applied to this view
      attr_accessor :global_spec_modifier_applied
    
      def initialize(args = {})
        super(args)
        @content            = args[:content] || [] # Will hold array of Gui::Widget
        @num_action_buttons = 0
        @data_classifier    = args[:classifier] || Object
        @view_name          = args[:view_name] || "#{self.class.to_s}#{SecureRandom.hex(8)}".to_sym
        @lazy_load_content  = args[:lazy_load_content] || false
        @hide_header        = args[:hide_header].nil? ? false : args[:hide_header]
        
        # Automatically inherit from parent class's specs if AUTO_INHERIT_SPECS is true
        if defined?(AUTO_INHERIT_SPECS) && AUTO_INHERIT_SPECS
          @data_classifier.immediate_parents.each { |parent| inherits_from_spec(self.view_name, parent) }
        end
        
        @object_view_name = args[:object_view_name] || Gui.option(:default_view_name)
        @object_view_type = args[:object_view_type] || Gui.option(:default_view_type)
      end
      
      def add_or_replace_widget(widget)
        widget_index = @content.index { |w| w.covers?(widget) }
        if widget_index
          @content[widget_index] = widget
        else
          @content << widget
        end
        @content
      end

      def all_parents
        return [] unless self.parent
        [self.parent] + parent.all_parents
      end
      
      # These are eval'ed at runtime (so the current value of 'Gui.loaded_spec.default_view_name' is used)
      attr_writer   :object_view_name
      def object_view_name
        @object_view_name.is_a?(String) ? eval(@object_view_name) : @object_view_name
      end
      attr_writer   :object_view_type
      def object_view_type
        if @object_view_type
          @object_view_type.is_a?(String) ? eval(@object_view_type) : @object_view_type
        end
      end
      # A Classifier (or String or Symbol naming a classifier) used to determine if the View is suitable for a given current_object
      attr_writer     :data_classifier
      attr_accessor   :through_classifier
      attr_accessor   :derived_parent_through_classifier
      def data_classifier;    GuiSpec.resolve_classifier(@data_classifier); end
      def through_classifier; GuiSpec.resolve_classifier(@through_classifier); end
      
      def view;   self; end
      def getter; nil;  end
      
      def view_type
        self.class.name.split('::').last.to_sym
      end

      def inspect
        output = ["#{self.class.name.split('::').last}: #{@data_classifier || 'data_classifier is nil'}"]
        output << @content.map { |c| c.inspect.indent }.join("\n") if @content
        output << ' (manually_sorted)' if manually_sorted
        output.join("\n")
      end

      def default_sort
        # Put view_refs last
        view_refs, other_widgets = @content.partition { |w|w.is_a?(ViewRef) }
        view_refs.sort_by! { |vr| vr.label.to_s }
        @content.replace(other_widgets + view_refs)
        
        # Put 'More Information' links last
        # NOTE: apparently 'More Information' is a default value given if the label is empty. -SD
        # @content.sort_by! { |w| (w.is_a?(Link) && w.label == '').to_s }
        more_info_link = @content.find { |w| (w.is_a?(Link) && w.label == '') }
        if more_info_link
          @content.delete(more_info_link)
          @content.push(more_info_link)
        end
      end
      
      def self.dsl_parameters
        [:view_name, :classifier]
      end
      
      def self.dsl_aliases
        []
      end
    end
  end
end
