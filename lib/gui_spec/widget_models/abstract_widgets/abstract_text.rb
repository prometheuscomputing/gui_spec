require_relative 'simple_widget'

module Gui
  module Widgets
    # Represents multiple lines of (possibly) styled text.
    class AbstractText < SimpleWidget
      include Collectionable
      
      # One of :Plain, :Markdown, :LaTeX, or something that will resolve_in to one of those
      # May eventually include :Textile, :Creole, :Ruby, :Java, :Haml etc.
      # The selected language for this text (set from the spec vs. set from the domain object)
      attr_accessor :language
      # All possible languages for this text
      attr_accessor :languages
      # Whether or not to show the language selection on a page
      attr_accessor :show_language_selection
      attr_accessor :always_collapsed
      attr_accessor :save_form_field_draft
            
      def initialize(args = {})
        super(args)
        @show_language_selection = args[:show_language_selection]
        @search_filter           = {:field => :content, :filter_type => :case_insensitive_like, :field_type => :complex_attribute}
        @save_form_field_draft   = args[:save_form_field_draft]
        @always_collapsed = args[:always_collapsed]
      end
    
      def default_language
        self.class.default_language
      end
    
      def default_language_string
        self.class.default_language_string
      end
      
      class << self
      
        def default_language_string
          default_language.respond_to?(:value) ? default_language.value : default_language.to_s
        end

      end
    end
  end
end
