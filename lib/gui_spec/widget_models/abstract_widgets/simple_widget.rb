require_relative 'widget'

module Gui
  module Widgets
    class SimpleWidget < Widget
      attr_accessor :getter, :setter
      attr_accessor :required
      attr_accessor :multiple
      attr_accessor :search_filter
      attr_accessor :decorator
      attr_accessor :hover # This is the title of the html tag.  It gets used for tooltips and error message defaults for validation.
      attr_accessor :table_name # Used in the case of association_class_widget's to qualify filters
      attr_accessor :default # Note that this default is a 'display' default, and is not saved.  It is used when the domain obj is not present
      attr_accessor :help_html #This displays a help icon if it is present.  When selected, it will display this html.
      
      
      #HTML5 Validation Attributes
      attr_accessor :required_html
      attr_accessor :regex_pattern
      attr_accessor :auto_focus
      
      def initialize(args = {})
        super
        @getter                   = args[:getter]&.to_s
        @setter                   = args[:setter]
        @label                  ||= default_label # Derive label from getter/setter if not explicitly defined
        @required                 = args[:required]
        @multiple                 = args[:multiple]
        @decorator                = args[:decorator]
        @hover                    = args[:hover] || args[:title]
        @help_html                = args[:help_html]
        @table_name               = args[:classifier] ? args[:classifier].table_name : args[:table_name]
        @search_filter            = args[:search_filter] || :case_insensitive_like
        @default                  = args[:default]
        @auto_focus               = args[:auto_focus]
        
        # Ugly...ViewRefs should really be more distinguishable from widgets that map to attribute type data
        # FIXME Gui::Widgets::ViewRef was Gui::Widget::ViewRef (note Widgets vs Widget).  This was almost certainly a mistake because self could NEVER have been a Gui::Widget::ViewRef...but who knows if "fixing" this will break things....in the meantime, we're thinking of changing Gui::Widget to Gui::WidgetControllers or something less confusing / error prone.
        @disabled                 = true if args[:view_only] && self.class != Gui::Widgets::ViewRef
        
        #HTML5 Attributes
        @required_html            = args[:required_html]
        @regex_pattern            = args[:regex_pattern]
      end
            
      def default_label
        getter ? getter.split(/[_\.]/).map { |word| word.capitalize }.join(" ") : ''
      end
      
      # Class methods
      class << self
        
        def dsl_parameters; [:getter]; end
        
        def dsl_aliases; []; end
        
        def canonical_column_name; nil; end

        # This is the entry point into any individual widget being responsible for telling us if there is a concurrency issue or not.  As of Feb. 2016, most widgets will still use the logic that has been in place for a while.  Gui::Widgets::File will implement its own logic.
        def check_concurrency(domain_obj:, getter:, current_value:, widget_info:, opts:{})
          answer = {}
          widget_info['data'].each do |key, submitted_value|
            initial_value = widget_info['initial_data'][key]        
            if initial_value || widget_info['last_update']
              if initial_value.is_a?(String)
                # Encoding conversion only necessary when comparying variables to values in database.
                initial_value.force_encoding('utf-8') 
                initial_value = Gui::Utils.remove_carriage_returns(Gui::Utils.trim_leading_and_trailing_newlines(initial_value))
              end
              opts.merge!({:key => key})
              begin
                concurrency_check = check_attribute_concurrency(domain_obj:domain_obj, getter:getter, current_value:current_value, initial_value:initial_value, submitted_value:submitted_value, opts:opts)
                if concurrency_check[:problem]
                  concurrent_change = {key.to_sym => find_value_change(current_value, initial_value, submitted_value, opts)}
                  answer[:concurrency_issues] ||={}
                  answer[:concurrency_issues][widget_info['getter']] = concurrent_change
                  answer[:concurrency_issues][widget_info['getter']][:label]  = widget_info['label']
                  answer[:concurrency_issues][widget_info['getter']][:widget] = widget_info['widget']
                  answer[:concurrency_issues][widget_info['getter']][:hover]  = widget_info['hover']
                  # Will always be present if present on the widget template page.
                  answer[:concurrency_issues][widget_info['getter']][:delete] = widget_info['delete'] == true
                end
                # No way to use concurrency_messages at this time -MF Feb. 10, 2016
                if concurrency_check[:message]
                  answer[:concurrency_messages] ||= []
                  answer[:concurrency_messages] << concurrency_check[:message]
                end
                if concurrency_check[:warning]
                  answer[:concurrency_warnings] ||= []
                  answer[:concurrency_warnings] << concurrency_check[:warning]
                end
              rescue Gui::WidgetValueError => e
                answer[:errors] ||= []
                answer[:errors] << e.message
              end
            end
          end
          answer
        end

        def cast_value_for_setting(value = {}, opts = {})
          raise NotImplementedError, "All SimpleWidgets should implement cast_value_for_setting"
        end
        
        def empty_data?(data)
          x = data.nil? || data.empty? || data.values.empty?
          raise "Well, #{data} is actually empty!" if x
          x
        end
        
        private
        
        def format_value(value, opts = {})
          value
        end
        
        def check_attribute_concurrency(domain_obj:, getter:, current_value:, initial_value:, submitted_value:, opts: {})
          no_problem = {}
          return no_problem if domain_obj.new? # it is impossible for two users to concurrently be working on the same new object so there is no need to check if the value has changed.
          unless initial_value || opts[:last_update] # this should never happen because we check before calling the method.
            puts "Warning from #{self.class}#check_attribute_concurrency: no initial_value or last_update time was submitted for '#{domain_obj.class}##{getter}'"
            return no_problem
          end
          formatted_current_value = format_value(current_value, opts.merge({:formatting_context => :concurrency_check, :value_type => :current}))
          formatted_current_value = Gui::Utils.trim_leading_and_trailing_newlines(formatted_current_value) if formatted_current_value.is_a?(String)
          
          submitted_value   = format_value(submitted_value, opts.merge({:formatting_context  => :concurrency_check, :value_type => :submitted}))
          submitted_value   = Gui::Utils.trim_leading_and_trailing_newlines(submitted_value) if submitted_value.is_a?(String)
        
          # It is possible that concurrent changes were made to the same value, in which case everything is fine.
          return no_problem if identical_concurrent_changes?(formatted_current_value, submitted_value, opts)
        
          initial_value = format_value(initial_value, opts.merge({:formatting_context => :concurrency_check, :value_type => :initial}))
        
          decision = decide_if_value_has_changed(initial_value, formatted_current_value, submitted_value, opts)
          decision = parse_concurrency_warnings(decision, initial_value, submitted_value, current_value, opts)
          decision
        end # def check_attribute_concurrency

        def decide_if_value_has_changed(initial_value, current_value, submitted_value, opts)
          # Return false if both are nil, or if current is nil and the delete command was triggered for the original
          if ((initial_value.nil? || initial_value == '') && (current_value.nil?  || current_value == '')) || ((current_value.nil?  || current_value == '') && opts[:delete])
            {}
          # If one is nil, but the wasn't, then clearly the value has changed
          elsif initial_value.nil? || initial_value == '' || current_value.nil? || current_value == ''
            {:problem => true}
          else
            {:problem => !current_value.eql?(initial_value)}
          end
        end # def decide_if_value_has_changed
      
        def parse_concurrency_warnings(decision, initial_value, submitted_value, raw_current_value, opts)
          decision
        end

        def identical_concurrent_changes?(current_value, submitted_value, opts)
          submitted_value == current_value
        end
      
        # Function needs to return {original_value, current_value, new_value}
        def find_value_change(raw_current_value, initial_value, new_value, opts = {})
          current_value     = format_value(raw_current_value, opts.merge({:formatting_context => :find_value, :value_type => :current}))
          new_value         = format_value(new_value,         opts.merge({:formatting_context => :find_value, :value_type => :submitted}))
          {:initial_value => initial_value, :current_value => current_value, :new_value => new_value} 
        end

      end # end of class methods
    end
  end
end
