require_relative 'simple_widget'

module Gui
  module Widgets
    class Numeric < SimpleWidget
      # A Range
      attr_accessor :range
      attr_accessor :placeholder_text
      attr_accessor :min_value
      attr_accessor :max_value
      attr_accessor :save_form_field_draft
      def data_classifier; ::Numeric; end
      
      def initialize(args = {})
        super(args)
        @range            = args[:range] || -Infinity..Infinity
        @placeholder_text = args[:placeholder_text]
        @min_value        = args[:min_value]
        @max_value        = args[:max_value]
        @save_form_field_draft = args[:save_form_field_draft]
      end
      
      class << self
        def cast_value_for_setting(value = {}, opts = {})
          # TODO: Handle multiple
          # Remove comma separators. 
          value = value['value'].gsub(',', '')
          convert_to_type(value, opts)
        end

        private
        def convert_to_type(value, opts)
          return nil unless value
          return nil if value.is_a?(::String) && value.empty?
          begin
            _convert_to_type(value)
          rescue ArgumentError => ae
            puts ae.message
            type = type_name.gsub('_', ' ')
            an   = (type =~ /^[aeiou]/) ? 'an' : 'a'
            raise WidgetValueError, "Could not save the '#{opts[:label]}' field. Please enter #{an} #{type}."
          end
        end
              
        def format_value(value, opts = {})
          convert_to_type(value, opts)
        end
      end # end class methods      
    end
  end
end
