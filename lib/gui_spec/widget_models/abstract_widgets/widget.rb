module Gui
  module Widgets
    # Abstract
    class Widget
      attr_reader   :data_classifier 
      attr_accessor :label
      attr_accessor :expanded
      attr_accessor :hidden
      attr_accessor :style # A widget-specific styling option
      attr_writer   :default_display # A display default that may be displayed if the widget is not set
      attr_writer   :disabled
      # This is used by widgets that have multiple display types (e.g. :list and :table for document associations)
      attr_accessor :display_type
      attr_accessor :classes
      # The widget that contains the definition of this widget
      attr_accessor :parent
      
      # Everything needs to respond to this and it is possible that it is appropriate for all widgets anyway...maybe?
      attr_accessor :filter_value
      
      # Procs that are evaluated at render time to see if this widget should be shown or not.  When called, these procs take one arg. Presumably, we are passing the locals of the context in which it is being rendered to the proc as fodder for conducting the test(s).  IMPORTANT NOTE: procs must be able to handle being passed nil as an arg!
      attr_accessor :hide_if
      
      attr_writer   :default_display # A display default that may be displayed if the widget is not set
      attr_writer   :disable_add, :disable_create, :disable_remove, :disable_delete, :disable_traverse, :recursively_disabled
      alias_method :disable_addition=,  :disable_add=
      alias_method :disable_creation=,  :disable_create=
      alias_method :disable_removal=,   :disable_remove=
      alias_method :disable_deletion=,  :disable_delete=
      alias_method :disable_traversal=, :disable_traverse=
      
      def initialize(args = {})
        @data_classifier  = args[:classifier]  # This indicates what type of domain model the widget is responsible for 
        @label            = args[:label]
        @expanded         = args[:expanded] || true
        @hidden           = args[:hidden] || false
        @tests            = args[:tests] || {}
        disabilities      = Array(args[:disable]) + Array(args[:disabled])
        @disabled         = disabilities.include?(true) || disabilities.include?(:all)
        @disable_add      = @disabled || disabilities.include?(:add)    || args[:view_only]
        @disable_create   = @disabled || disabilities.include?(:create) || args[:view_only]
        @disable_remove   = @disabled || disabilities.include?(:remove) || args[:view_only]
        @disable_delete   = @disabled || disabilities.include?(:delete) || args[:view_only]
        @disable_traverse = @disabled || disabilities.include?(:traverse)
        @hide_if          = [args[:hide_if]].flatten if args[:hide_if]
        @style            = args[:style]
        @default_display  = args[:default_display]
        @display_type     = args[:display_type]
        @classes          = args[:classes] || []
        @parent           = args[:parent]
        @filter_value     = args[:filter_value]
        # Whether this widget is not directly for the domain object but instead for the association class object
        @association_class_widget = args[:association_class_widget]
      end

      def inspect
        "#{self.class.name.split('::').last}: #{@getter}--#{@label}#{'(hidden)' if @hidden}"
      end
      
      def hidden?(locals = nil)
        @hidden || test_hide_if(locals)
      end
      
      def test_hide_if(locals)
        return false unless @hide_if
        @hide_if.find { |prok| prok.call(locals) }
        # is_hidden = @hide_if.find do |prok|
        #   answer = prok.call(locals)
        #   puts Rainbow("Prok answer is #{answer}").orange
        #   answer
        # end
        # puts Rainbow("is_hidden is #{!!is_hidden}").red
        # is_hidden
      end
      private :test_hide_if
      
      attr_accessor :association_class_widget
      alias_method  :association_class_widget?, :association_class_widget
       
      def disabled?(*values)
        return true      if @recursively_disabled
        return @disabled if values.empty?
        values.each do |action|
          case action
          when :all
            answer = @disabled
          when :disable_addition
            answer = @disable_add 
          when :disable_deletion
            answer = @disable_delete
          when :disable_removal
            answer = @disable_remove
          when :disable_creation
            answer = @disable_create
          when :disable_traversal
            answer = @disable_traverse
          end
          return answer if answer
        end
        false
      end
      
      def disable(val = true)
        self.disabled = val
      end
      def disable=(val)
        self.disabled = val
      end
      def disabled= val
        @disabled = @disable_add = @disable_create = @disable_remove = @disable_delete = @disable_traverse = val
      end
      
      def view_only=(val)
        @disable_add = @disable_create = @disable_remove = @disable_delete = val
      end
      
      def set_test(operation, test)
        @tests[operation] ||= []
        @tests[operation] << test unless @tests[operation].include?(test)
      end
      
      def get_tests(operation = nil)
        return @tests unless operation
        begin
          @tests[operation]
        # OK to rescue exception because we re-raise
        rescue StandardError => e
          puts operation
          inspect_tests
          pp info # in rescue
          raise e
        end
      end
      
      def inspect_tests
        puts self.to_s + " " + self.getter.to_s
        puts @tests.pretty_inspect
      end
      
      def default_display
        @default_display || "[No #{label}]"
      end
      
      # Alternate accessor for label to distinguish from view_dsl method
      def widget_label
        @label
      end
      
      # Custom decorator class methods
      @@custom_decorators = {}
      def self.add_custom_decorators name_proc_hash
        @@custom_decorators.merge!(name_proc_hash)
      end
      def self.clear_custom_decorators
        @@custom_decorators = {}
      end
      def self.custom_decorators
        @@custom_decorators
      end
      # Convenience method to access at instance level
      def custom_decorators
        @@custom_decorators
      end
    
      # Check for equivalent values and class
      def eql?(other_widget)
        self.class == other_widget.class && self == other_widget
      end
    
      # Test for equality of values (no class check)
      def ==(other_widget)
        # Use reflection to determine if instance variables are all equal for both objects
        self.instance_variables.inject(true) do |equality_value, instance_variable|
          equality_value && 
            other_widget.instance_variables.include?(instance_variable) &&
            self.instance_variable_get(instance_variable) == other_widget.instance_variable_get(instance_variable)
        end
      end
    
      # this method is used to #sort a collection/organizer widget.
      # Currently this is an example implementation that doesn't look too great.
      def <=>(other_widget)
        if self.class == other_widget.class
          self.label <=> other_widget.label
        else
          self.class.to_s <=> other_widget.class.to_s
        end
      end
    
      # Test for partial equality. Useful for determining whether two widgets are covering the same getter/action (and thus should be mutually exclusive).  
      # NO, they should not be mutually exclusive just because they have the same getter! It is perfectly reasonable to have multiple viewrefs that have the same getter. - MF
      def covers?(other_widget)
        return false unless self.class == other_widget.class
        if self.is_a?(Gui::Widgets::ViewRef)
          self.id_string == other_widget.id_string && self.view_name == other_widget.view_name
        else
          self.id_string == other_widget.id_string
        end
      end
    
      def id_string
        "#{self.class}_#{self.getter}"
      end
    
      # Return a clone of this widget
      def clone
        args = self.is_a?(Gui::Widgets::ViewRef) ? {:getter => self.getter, :parent => self.parent} : {}
        clone_widget = self.class.new(args)
        clone_widget.setup_clone(self)
        clone_widget
      end
    
      # Set up the current clone with the given widget's parameters
      def setup_clone(orig_widget)
        orig_widget.instance_variables.each do |instance_variable|
          orig_instance_var_value = orig_widget.instance_variable_get(instance_variable)
          next if orig_instance_var_value.nil? # there are times when false means something different than nil
          # Set the clone's instance variable to a deep clone of the original value
          self.instance_variable_set(instance_variable, _get_obj_clone(orig_instance_var_value))
        end
      end
    
      def _get_obj_clone(object)
        case object
        when Array
          object.map { |o| _get_obj_clone(o) }
        when Hash
          clone_hash = {}
          object.each { |key, value| clone_hash[key] = _get_obj_clone(value) }
          clone_hash
        else
          object
        end
      end
      
      def info
        data = {:widget_class => self.class, :label => label}
        data[:parent_info] = {:data_classifier => parent.data_classifier} if parent
        instance_variables.each do |v|
          next if [:@parent, :@view, :@content, :@_haml_locals, :@haml_buffer].include?(v)
          val = instance_variable_get(v)
          data[v] =  val unless val == nil || val == []
        end
        data
      end
      
      def self.type_name
        name.split('::').last.to_snakecase
      end
      
      def context?
        false
      end
    end
  end
end
