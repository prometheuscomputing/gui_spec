require_relative 'view'

# Really want to separate the functionality of styling and context switching.  Essentially, we want a Context that doesn't take a getter and just acts as a wrapper / decorator within the current context.  This probably means that it is still a View

module Gui
  module Widgets
    class Style < View
      attr_accessor :style_options # Note: can't be named #style due to conflict w/ DSL method
      attr_accessor :style_options_label # Note: can't be named #style due to conflict w/ DSL method

      def initialize(args = {})
        super(args)
        @style_options       = args[:style]       || {}
        @style_options_label = args[:label_style] || {}
      end
      def view_type; :Style; end
    end
    
  end
end
