require_relative 'simple_widget'

module Gui
  module Widgets
    # Represents a hidden input field.
    # Is unused as far as I can tell - S.D.
    class Hidden < SimpleWidget
      def data_classifier; ::Object; end

    end
  end
end