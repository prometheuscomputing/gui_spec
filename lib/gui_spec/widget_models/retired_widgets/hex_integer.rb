require_relative 'integer'

module Gui
  module Widgets
    class HexInteger < Integer
      include Collectionable
    end
  end
end
