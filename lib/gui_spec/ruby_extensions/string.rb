class String
  def to_boolean
    return nil if self.empty?
    return true if self =~ (/(true|t|yes|y|1)$/i)
    return false if self.empty? || self =~ (/(false|f|no|n|0)$/i)
    raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
  end
end