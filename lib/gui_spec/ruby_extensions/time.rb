class Time
  def to_s_usec
    self.strftime("%Y-%m-%d %H:%M:%S") + "." + self.usec.to_s + self.strftime("%z")
  end
end