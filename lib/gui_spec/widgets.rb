# require this file if you want the widgets without any DSL...
require 'gui_director/options'
require 'gui_director/ruby_extensions/string'
# NOTE the ordering of these requires matters.  Do not change it.
require_dir relative('widget_models/mixins')
require_dir relative('widget_models/abstract_widgets')
require_dir relative('widget_models/view_widgets')
require_dir relative('widget_models/basic_widgets')
require_relative 'registry'

module Gui::Widgets
    # Get a widget instance based on the widget's type. Give new widget a label if @label is passed in.
  # TODO: remove reliance on creating widget instance
  def self.get_widget(widget_type, label = nil)
    klass = get_widget_class(widget_type)
    klass ? klass.new(:label => label) : nil
  end
  
  def self.get_widget_class(widget_type_string)
    klass = GuiSpec.registry.dig(widget_type_string.to_s, :class)
    # You should probably expect an exception downstream if klass == nil
    puts "!!!! Unable to determine widget class for: #{widget_type_string.inspect}" unless klass
    klass
  end
end
