module GuiSpec

  GEM_NAME    = 'gui_spec'
  VERSION     = '1.8.0'
  AUTHORS     = ["Samuel Dana", "Michael Faughn"]
  EMAILS      = ["s.dana@prometheuscomputing.com", "michael.faughn@nist.gov"]
  HOMEPAGE    = 'https://gitlab.com/prometheuscomputing/gui_spec'
  SUMMARY     = %q{Widget models and DSL for use in the Prometheus Computing GuiBuilder toolchain.}
  DESCRIPTION = SUMMARY
  LANGUAGE    = :ruby
  RUNTIME_VERSIONS   = { :mri => ['> 2.7'] }
  LANGUAGE_VERSION   = ['>= 2.7']
  TYPE               = :library
  LAUNCHER           = nil
  DEPENDENCIES_RUBY  =  { 
    :lodepath    => '~> 0.0',
    :cleanroom   => '~> 1.0',
    :indentation => '~> 0.0',
  }
  DEVELOPMENT_DEPENDENCIES_RUBY  = {}
end
