require_relative 'widgets'
require 'cleanroom'

require 'gui_spec/helpers/resolve_classifier'

require 'gui_spec/dsl/base_dsl'
require 'gui_spec/dsl/dsl_context_helpers'
require 'gui_spec/dsl/modifier_dsl'
require 'gui_spec/dsl/widget_dsl'
require 'gui_spec/dsl/manually_defined_widget_dsl'
require 'gui_spec/dsl/metacoded_widget_dsl'
require 'gui_spec/dsl/inject_dsl_into_view'
