class Gui::Widgets::View
  # Create proxies to DSL methods, so they can be called on a view.
  # The only place this behavior is used (that I'm aware of) is in global_spec_modifiers. -SD
  
  # NOTE: must manually remove :label, since there is already a label method defined on view, which this definition would override. -SD
  # TODO: find a better way to write global spec modifiers that doesn't need this hacky method -SD
  (GuiSpec::DSL.exposed_methods.keys - [:label]).each do |meth|
    define_method(meth) do |*args|
      context_stack = self.parent
      dsl = GuiSpec::DSL.new(:additional_context => all_parents)
      dsl.evaluate_context_for(self, all_parents) { send(meth, *args) }
    end
  end
end