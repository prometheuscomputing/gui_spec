module GuiSpec
  class DSL
    def homepage_item(type, args = {})
      Gui::Home.add_methods(type, args)
    end
    expose :homepage_item

    def covered_packages(*args)
      # Supports loading of models from multiple generated gems
      @spec.covered_packages ||= []
      @spec.covered_packages.concat(args)
    end
    expose :covered_packages

    def summary_view(classifier, args = {}, &block)
      view_name = args[:view_name] || :Summary
      organizer(view_name, classifier, args, &block)
      collection(view_name, classifier, args, &block)
    end
    expose :summary_view

    def detail_view(classifier, args = {}, &block)
      view_name = args[:view_name] || :Details
      organizer(view_name, classifier, args, &block)
    end
    expose :detail_view
    
    def number(*args)
      Kernel.warn("GuiSpec::DSL#number is deprecated!  Use #float.")
      float(*args)
    end
    expose :number
    
    def fixedpoint(*args)
      Kernel.warn("GuiSpec::DSL#fixedpoint is deprecated!  Use #float.")
      float(*args)
    end
    expose :fixedpoint
    
    def hex_integer(*args)
      Kernel.warn("GuiSpec::DSL#hex_integer has been retired.  Creating regular integer instead!")
      integer(*args)
    end
    expose :hex_integer

    # Style is still experimental
    # def style(options = {}, &block)
    #   add_widget(Gui::Widgets::Style, [{:style => options, :parent => parent_from_context}], :add_widget, Gui::Widgets::View, &block)
    # end
    # expose :style
  end
end
