# DSL available from within a view
# # # public methods: # # #
# inherits_from_spec
# attributes_from
# view_refs_from
# content_from
# relabel
# ordered -- no test for this but it's a super-simple method...
# order
# reorder
# sort -- needs test
# sort_by -- needs test
# expand
# expand_if -- NOT IMPLEMENTED downstream!
# remove (aka hide)
# remove_if (aka hide_if)
# remove_all (aka hide_all)
# remove_all_if (aka hide_all_if)
# disable
# disable_all
# disable_addition
# disable_removal
# disable_creation
# disable_deletion
# disable_traversal
# disable_if
# disable_all_if
# disable_addition_if
# disable_removal_if
# disable_creation_if
# disable_deletion_if
# disable_traversal_if
# recursively_disable -- needs test
# view_only           -- needs test
# view_only_if        -- needs test
# view_only_all       -- needs test
# view_only_all_if    -- needs test
# enable
# enable_addition
# enable_removal
# enable_creation
# enable_deletion
# enable_traversal
# hide_types
# hover

module GuiSpec
	class DSL
    def debug
      pp content
    end
    expose :debug
    
		def attributes_from(classifier, identifier: :Summary, **opts)
      identifier = opts[:view_name] if opts[:view_name] # preserve old key name for consistency across methods
      view_type = @context.last.class.name.split('::').last.to_sym
      view = @spec.retrieve_exact_view(identifier, classifier, view_type)
      if view
        _copy_content_from(view)
      else
        raise "No #{view_type} for #{classifier} with view_name: #{identifier}"
      end
    end
    expose :attributes_from

    # Unused, as far as I can tell -SD
    def view_refs_from(classifier, identifier: :Details, view_type: :Organizer, **opts)
      identifier = opts[:view_name] if opts[:view_name] # preserve old key name for consistency across methods
      view = @spec.retrieve_exact_view(identifier, classifier, view_type)
      if view
        _copy_content_from(view, :view_refs => true, :attributes => false)
      else
        raise "No #{view_type} for #{classifier} with identifier: #{identifier}"
      end
    end
    expose :view_refs_from

    def inherits_from_spec(parent_view_name, parent_data_classifier = nil, parent_view_type = nil, inherit_view_refs = true)
      parent_view_name       ||= @context.last.view_name
      parent_data_classifier ||= @context.last.data_classifier
      parent_view_type       ||= @context.last.class.name.split('::').last.to_sym
      parent_view = @spec.retrieve_view(parent_view_name, parent_data_classifier, parent_view_type)
      if parent_view
        _copy_content_from(parent_view, :view_refs => inherit_view_refs)
      else
        puts "Unable to set up spec inheritance from (#{parent_view_name}, #{parent_data_classifier}, #{parent_view_type}) for spec (#{parent_view_name}, #{parent_data_classifier}, #{parent_view_type})"
      end
    end
    expose :inherits_from_spec
    
    def content_from(classifier, identifier: nil, view_type: nil, **opts)
      identifier ||= opts[:view_name] || @context.last.view_name
      classifier ||= @context.last.data_classifier
      view_type  ||= @context.last.class.name.split('::').last.to_sym
      view = @spec.retrieve_exact_view(identifier, classifier, view_type)
      if view
        _copy_content_from(view, :view_refs => true)
      else
        raise "No #{view_type} for #{classifier} with view_name: #{identifier}"
      end
    end
    expose :content_from

		def _copy_content_from(view, opts = {})
      include_view_refs = opts[:view_refs]
      include_attributes = true unless opts[:attributes] == false
      new_content = []      
      view.content.each do |w|
        if w.is_a?(Gui::Widgets::ViewRef)
          if include_view_refs
            new_content << w.clone
          end
        else # it is an attribute
          if include_attributes
            new_content << w.clone
          end
        end
      end
      new_content = new_content + content
      new_content.uniq! { |w| w.id_string }
      # Not sure we still need to replace here.
      content.replace(new_content)
    end

    def relabel(widget_getter, label)
      modify(widget_getter, {:label => label})
    end
    expose :relabel

    def remove(*widget_getters)
      widget_getters.each { |widget_getter| modify(widget_getter, {:remove => true}) }
    end
    expose :remove
    alias_method :hide, :remove
    expose :hide

    # TODO: We want to be able to have self hold onto the test so that it can be performed only once per page load and then the result used to determine if the widgets corresponding to the passed in widget_getters should be displayed or not.  This should be the default behavior.  The alternative ':widget_dynamic' behavior would have the test stored on each of the widgets corresponding to the passed in widget_getters.  The same would apply to #remove_all_if, #delete_if, and #delete_all_if.  Implementing this will represent a bit of work.
    def remove_if(test, *widget_getters)
      if widget_getters.last.is_a?(Hash)
        opts = widget_getters.pop
      end
      # if opts[:widget_dynamic]
        # TODO this is the :widget_dynamic behavior and is the only thing implemented at this point
        widget_getters.each { |widget_getter| modify(widget_getter, {:remove_if => test}) }
      # else
        # TODO implement me
      # end
    end
    expose :remove_if
    alias_method :hide_if, :remove_if
    expose :hide_if

    def remove_all
      # content.each { |widget| modify(widget.getter, {:remove => true}) } # backasswards...just to use #modify
      content.each { |widget| widget.hidden = true } # reasonable
    end
    expose :remove_all
    alias_method :hide_all, :remove_all
    expose :hide_all

    def remove_all_if(test)
      content.each { |widget| widget.set_test(:hide, test) }
      # TODO handle columns for collections
    end
    expose :remove_all_if
    alias_method :hide_all_if, :remove_all_if
    expose :hide_all_if

    # Define enable*/disable*/disable*_if methods
    types = [[:disable], [:disable, :if], [:enable]]
    actions = [:all, :addition, :removal, :creation, :deletion, :traversal]

    types.each do |prefix, suffix| 
      actions.each do |action|
        method_name = "#{prefix}"
        method_name << "_#{action}" unless action == :all
        method_name << "_#{suffix}" if suffix
        define_method(method_name) do |*args|
          test = args.shift if suffix == :if
          disable_args = [args]
          disable_args << (prefix == :disable)
          disable_args << action
          disable_args << test if test
          _disable(*disable_args)
        end
        expose method_name
      end
    end

    def disable_all
      _disable_widgets(content, true, :disable)
    end
    expose :disable_all

    def disable_all_if(test)
      _disable_widgets(content, true, :disable, test)
    end
    expose :disable_all_if

    def recursively_disable(*widget_getters)
      disable(*widget_getters)
      _widgets(widget_getters).each { |w| w.recursively_disabled = true }
    end
    expose :recursively_disable

    # Note that widget getters definitely is (and must be) an array by the time this method is called
    def _disable(widget_getters, bool, operation, test = nil)
      _disable_widgets(_widgets(widget_getters), bool, operation, test)
    end

    def _disable_widgets(widgets, bool, operation, test = nil)
      operation = operation == :all ? :disable : "disable_#{operation}".to_sym
      widgets = [widgets] unless widgets.is_a?(Array)
      widgets.each do |w|
        if test
          w.set_test(operation, test)
        else
          w.send("#{operation}=", bool)
        end
      end
    end

    def read_only
      view_only_all
      parent_from_context.disabled = true
    end
    expose :read_only
    
    def read_only_if(test)
      view_only_all_if(test)
       _view_only_if(test, parent_from_context)
    end
    expose :read_only_if
    
    def disable_all_if(test)
      _disable_widgets(content, true, :disable, test)
    end
    expose :disable_all_if

    def view_only(*widget_getters)
      _view_only(_widgets(widget_getters))
    end
    expose :view_only

    def view_only_if(test, *widget_getters)
       _view_only_if(test,_widgets(widget_getters))
    end
    expose :view_only_if

    def view_only_all
      _view_only(content)
    end
    expose :view_only_all
    
    def view_only_all_if(test)
      _view_only_if(test, content)
    end
    expose :view_only_all_if
    
    def _view_only(widgets)
      widgets.each do |w|
        if w.class == Gui::Widgets::ViewRef
          _disable(w.getter, true, :addition)
          _disable(w.getter, true, :removal )
          _disable(w.getter, true, :creation)
          _disable(w.getter, true, :deletion)
        else
          _disable(w.getter, true, :all)
        end
      end
    end

    def _view_only_if(test, widgets)
      widgets.each do |w|
        if w.class == Gui::Widgets::ViewRef
          _disable_widgets(w, true, :addition, test)
          _disable_widgets(w, true, :removal,  test)
          _disable_widgets(w, true, :creation, test)
          _disable_widgets(w, true, :deletion, test)
        else
          _disable_widgets(w, true, :all, test)
        end
        # TODO what if _view_only_if is appied to a Context widget??
      end
    end

    def expand(*widget_getters)
      widget_getters.each { |widget_getter| modify(widget_getter, {:expanded => true}) }
    end
    expose :expand
    
    def expand_if(test, *widget_getters)
      if widget_getters.last.is_a?(Hash)
        opts = widget_getters.pop
      end
      # if opts[:widget_dynamic]
        # TODO this is the :widget_dynamic behavior and is the only thing implemented at this point
        widget_getters.each { |widget_getter| modify(widget_getter, {:expand_if => test}) }
      # else
        # TODO implement me
      # end
    end
    expose :expand_if
    
    def hover(widget_getter, text)
      w = _widgets(widget_getter).first
      w.hover = text if w
    end
    expose :hover
    
    def ordered
      self.manually_sorted = true
    end
    expose :ordered
    
    def order(*widget_getters)
      self.manually_sorted = true
      new_contents = []
      widget_getters.each { |getter| new_contents += _widgets(getter) }
      new_contents.uniq!
      content.replace(new_contents + (content - new_contents))
    end
    expose :order
    
    def reorder(*widget_getters, options)
      self.manually_sorted = true
      widgets = _widgets(widget_getters)
      widgets.each { |w| content.delete(w) }
      if options[:to_beginning]
        content.replace(widgets + content)
      elsif options[:after]
        after_widgets = _widgets(options[:after])
        raise "Could not reorder because after_widgets could not be found: #{options[:after]}" unless after_widgets.any?
        after_index = content.index(after_widgets.last)
        content.insert(after_index + 1, *widgets)
      elsif options[:before]
        before_widgets = _widgets(options[:before])
        raise "Could not reorder because before_widgets could not be found: #{options[:before]}" unless before_widgets.any?
        before_index = content.index(before_widgets.first)
        content.insert(before_index, *widgets)
      elsif options[:to_end]
        content.replace(content + widgets)
      end # Do nothing if no valid option specified
    end
    expose :reorder
    
    def sort(&block)
      self.manually_sorted = true
      if block_given?
        content.sort! &block
      else
        content.sort!
      end
    end
    expose :sort
    
    def sort_by(&block)
      self.manually_sorted = true
      if block_given?
        content.sort_by! &block
      else
        content.sort_by!
      end
    end
    expose :sort_by
    
    def hide_types(*widget_getters)
      if widget_getters.any?
        # this finds the view_refs
      else
        # this works by putting the directive directly on the collection
        hide_types = true if respond_to?(:hide_types)
      end      
    end
    expose :hide_types

    def apply_global_spec_modifiers
      global_spec_modifiers = @spec.global_spec_modifiers
      return unless global_spec_modifiers&.any?
      global_spec_modifiers.each { |gsm| gsm.call(self) }
    end
    expose :apply_global_spec_modifiers

    def destroy(view_name, classifier, view_type, args = {})
      @spec.destroy_exact_view(view_name, classifier, view_type)
    end
    expose :destroy

    # This will be used by a #sort_by block and not a #sort block
    def default_view_sort(&block)
      if block_given? # Set the default view sort
        @spec.default_view_sort = block
        # Sort all existing views with the new sort
        @spec.sort_views
      else # Just return the current value
        @spec.default_view_sort
      end
    end
    expose :default_view_sort

    # Set a global spec modifier
    def global_spec_modifier(&block)
      @spec.global_spec_modifiers << block
    end
    expose :global_spec_modifier

    def modify(widget_getter, options)
      _modify(_widgets(widget_getter), options)
    end
    expose :modify
    
    def _modify(widgets, options)
      [widgets].flatten.each do |w|
        if options[:remove]
          w.hidden = true
        elsif options[:remove_if]
          w.set_test(:hide, options[:remove_if])
        elsif options[:expand_if]
          w.set_test(:expand, options[:expand_if])         
        else
          w.label      = options[:label] if options[:label]
          w.disabled   = true  if options[:disable]
          w.disabled   = false if options[:enable]
          w.expanded   = true  if options[:expanded]
          w.hide_types = true  if options[:hide_types]
        end
      end
    end

    # TODO: replace this with object oriented code in widgets to determine whether a string matches a widget
    def _widgets(*widget_getters)
      widget_getters.flatten!
      if widget_getters.length == 1
        widget_getter = widget_getters.first
      else
        return widget_getters.map { |wg| _widgets(wg) }.flatten.uniq
      end
      if widget_getter.is_a?(::String)
        widgets = content.select { |w| w.getter    == widget_getter }
        widgets = content.select { |w| w.id_string == widget_getter } if widgets.empty?
        widgets = content.select { |w| w.label     == widget_getter } if widgets.empty?
      elsif widget_getter.is_a?(::Regexp)
        widgets = content.select { |w| w.getter.to_s    =~ widget_getter }
        widgets = content.select { |w| w.id_string.to_s =~ widget_getter } if widgets.empty?
        widgets = content.select { |w| w.label.to_s     =~ widget_getter } if widgets.empty?
      elsif widget_getter.is_a?(::Proc)
        widgets = content.select { |w| widget_getter.call(w) }
      else
        widgets = []
      end
      widgets      
    end
    
	end
end
