# This file contains the widget creation DSL that is common to both manually defined and metacoded widget creation methods
module GuiSpec
	class DSL
    # TODO move this to a more general location (gui_site's #default_app_options or somehwere in gui_director?)
    DEFAULT_SEARCH_FILTER_TYPE = :case_insensitive_like
    
		# Interpret a DSL element into a widget
    def add_widget(parent_widget, widget_class, initialize_args, context_add_method, &block)      
      # puts "Adding a #{widget_class} with #{initialize_args.inspect}"
      # Initialize or retrieve the widget. For new views, store in the Spec
      # pp initialize_args if widget_class.name =~ /Context/
      widget = get_widget(widget_class, initialize_args)
      raise "Got nil widget for #{widget_class} with args: #{initialize_args.inspect}" unless widget
      # Add the widget to the parent context
      add_to_parent_context(parent_widget, widget, context_add_method) if parent_widget
      # Evaluate the passed block in the context of this element
      evaluate_context_for(widget, &block) if block
      widget
    end

    # Lookup existing widget (for views) or create new widget with passed arguments
    # FIXME this is bad because if you want to modify this widget in custom spec by passing new args, you can't. Why? because it is just going to grab the existing widget instead of creating a new one.
    def get_widget(widget_class, args = {})
      if widget_class.ancestors.include?(Gui::Widgets::View)
        view_type = widget_class.name.to_s.split('::').last.to_sym
        widget = @spec.retrieve_exact_view(args[:view_name], args[:classifier], view_type)
        unless widget
          widget = widget_class.new(args)
          # Default view_name to randomly generated name
          widget.view_name ||= "#{view_type.to_s}#{SecureRandom.hex(8)}".to_sym
          # Store view in spec if classifier is set
          @spec.store_view(widget) if widget.data_classifier
        end
        widget
      else
        begin
          widget_class.new(args)
        rescue
          pp args # in rescue
          raise
        end
      end
    end

    # Evaluate the passed block in the context of the passed widget
    # Optional context_stack argument allows overriding of current context stack (@context)
    def evaluate_context_for(widget, context_stack = nil, &block)
      context_stack = @context
      if context_stack
        old_context = @context.dup
        @context.replace(context_stack)
      end
      @context.push(widget)
      
      # Invoke Cleanroom DSL evaluation for passed block
      self.evaluate(&block)

      @context.pop
      # Restore old context if necessary
      @context.replace(old_context) if context_stack
      # Sort widget if necessary FIXME this should probably not be happening!!!!
      spec.sort_view(widget)
    end
    
    # Add an widget to the current context
    def add_to_parent_context(parent, widget, method = nil)
      # Determine add method and check for validity of context type
      raise "A #{widget.class} may only be defined as a top level widget" unless method

      # expected_parent_type = widget.class < Gui::Widgets::View ? nil : Gui::Widgets::View
      
      # Get current context
      # parent = parent_from_context
      
      if widget.is_a?(Gui::Widgets::SimpleWidget) && !parent.is_a?(Gui::Widgets::View)
        raise "You may only define a #{widget.class} within a #{expected_context_type}. Attempted to add #{widget.class} to a #{parent.class}"
      end
      
      # Add the widget to the context using the specified method
      parent.send(method, widget) if parent
      widget
    end
	end
end