require 'common/constants'

# HEY YOU!  Look at the end of this file.  That is where the DSL method creation gets kicked off.
module GuiSpec  
  class DSL
    NO_DSL_FOR = [Gui::Widgets::AbstractText, Gui::Widgets::Numeric]
    
    # Define the DSL method that specifies the given widget class
    # Params notes:
    #   @canonical_column_name - Since columns only support one 'canonical' name for a widget, this name will be used in place of the actual method name when using legacy column defns
    def self.define_widget_dsl(klass)
      name = klass.type_name

      aliases = klass.dsl_aliases
      underscore_method = "_#{name}".to_sym
      
      # Define context dependent method
      define_method(name) do |*args, &block|
        parent = parent_from_context
        # TODO we're going to try sending regular methods to Collection widgets as well.
        send(underscore_method.to_sym, parent, *args, &block)
      end
      expose name
      aliases.each { |a| alias_method a, name; expose a }
      
      # Define default widget method
      define_method(underscore_method)  do |parent, *params_args, &block|
        init_args = self.class.construct_widget_init_args(klass.dsl_parameters, params_args, parent)
        add_widget(parent, klass, init_args, :add_or_replace_widget, &block)
        # puts "Defined #{underscore_method} with init_args: #{init_args.inspect}"
      end
      expose underscore_method
      # aliases.each { |a| alias_method "_#{a}", underscore_method; expose "_#{a}" }
      
      ([name] + aliases).each do |n|
        if GuiSpec.registry[n.to_s]
          # Ramaze reloaded will cause this to explode so we have to check more carefully...
          unless GuiSpec.registry[n.to_s] == klass
            pp GuiSpec.registry
            raise "Cannot register #{n} with class #{klass} because it is already registered with class #{GuiSpec.registry[n.to_s]}."
          end
        else
          GuiSpec.registry[n.to_s] = {:class => klass}
        end
      end 
    end
    
    def self.construct_widget_init_args(parameters, params_args, parent)
      widget_init_args = {:parent => parent}
      widget_init_args.merge!(params_args.pop) if params_args.last.is_a?(Hash)

      raise "Unexpected Hash in params_args! #{params_args.inspect}" if params_args.find { |e| e.is_a?(Hash)}
        
      if parameters.size < params_args.size
        puts "parameters:  #{parameters}"
        puts "params_args: #{params_args}"
        raise "More parameter arguments than parameters!"
      end
      
      widget_init_args.merge!(Hash[parameters.zip(params_args)])
      widget_init_args
    end
  end
end

ObjectSpace.each_object(Class).select do |klass|
  next unless (klass < Gui::Widgets::SimpleWidget || klass < Gui::Widgets::View)
  next if GuiSpec::DSL::NO_DSL_FOR.include?(klass)
  GuiSpec::DSL.define_widget_dsl(klass)
end
