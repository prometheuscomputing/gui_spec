# This file contains DSL helper methods that refer back to the current context widget or context stack
module GuiSpec
	class DSL
    def content
      @context.last.content
    end

    def manually_sorted
      @context.last.manually_sorted
    end

    def manually_sorted=(value)
      @context.last.manually_sorted = value
    end

    def parent_from_context
      @context.last unless @context.last.is_a?(GuiSpec::Spec)
    end

  end
end