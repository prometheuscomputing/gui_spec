# backward compatibility patch
Home = Gui::Home

module GuiSpec
  class DSL
    include Cleanroom
    # Define instance-level spec for this DSL instance
    attr_accessor :spec

    def self.parse(dsl, file_path = nil, options = {})
      file_path ||= 'GuiSpec::DSL'
      dsl_instance = self.new(options)
      dsl_instance.evaluate(dsl, file_path, 1)
      dsl_instance.spec
    end
    
    def initialize(options = {})
      if options[:spec]
        # If passed a :spec option, use that spec
        @spec = options[:spec]
      else
        # Else create a new GuiSpec::Spec instance
        @spec = GuiSpec::Spec.new
      end
      # An array representing the context stack
      @context = [@spec]
      @context += options[:additional_context] if options[:additional_context]
    end
  end
end