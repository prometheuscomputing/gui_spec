# loads the widgets and the DSL
require 'lodepath'
require 'gui_spec/spec'
require 'gui_spec/dsl'
module GuiSpec
  
  # ----- Ruby DSL deserialization code -----
  # Deserialize GUI Spec from DSL string
  # parameters:
  #   - dsl - DSL string
  #   - file_path - path to display when showing stack traces for evaluated DSL
  def self.from_dsl(dsl, file_path = nil, options = {})
    spec = GuiSpec::DSL.parse(dsl, file_path, options)
    # Link view refs, apply then clear any global spec modifiers
    spec.resolve unless options[:no_resolve]
    spec
  end

  # Deserialize GUI Spec from DSL file
  def self.from_dsl_file(file_path, options = {})
    path = File.expand_path(file_path)
    self.from_dsl(File.read(path), path, options)
  end
  
  def self.from_dsl_files(file_paths, options = {})
    paths = file_paths.dup
    spec = from_dsl_file(paths.shift, options.merge(:no_resolve => true))
    options[:spec] ||= spec
    paths.each do |path|
      from_dsl_file(path, options.merge(:no_resolve => true))
    end
    # Link view refs, apply then clear any global spec modifiers
    spec.resolve unless options[:no_resolve]
    spec
  end
end