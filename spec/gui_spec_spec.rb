require 'spec_helper'

# TODO - text inherit_from_spec when parent spec includes contexts (including nested contexts)

describe 'gui_spec' do
  
  # before(:all) do
  #     load_carexample_models # Why?
  # end
  
  it 'should be able to load a context from within an organizer' do
    puts Rainbow('BEGIN!').cyan
    # simple
    @context_spec1 = GuiSpec.from_dsl_file(relative('../test_data/context_spec1.rb'))
    # multiple contexts referring to same object
    @context_spec2 = GuiSpec.from_dsl_file(relative('../test_data/context_spec2.rb'))
    # # indirectly nested contexts
    # @context_spec3 = GuiSpec.from_dsl_file(relative('../test_data/context_spec3.rb'))
    # directly nested contexts
    @context_spec4 = GuiSpec.from_dsl_file(relative('../test_data/context_spec4.rb'))
    # looped contexts (one-to-one)
    @context_spec5 = GuiSpec.from_dsl_file(relative('../test_data/context_spec5.rb'))
  end
  
  # This is pulled out from above because it is (currently) the only one that fails
  it 'should handle indirectly nested contexts' do
    # indirectly nested contexts
    pending('Fixing this...if we want to')
    @context_spec3 = GuiSpec.from_dsl_file(relative('../test_data/context_spec3.rb'))
  end
  

  # it 'should build a spec from a DSL file (tiny_test_spec)' do
  #     $debug = true
  #     GuiSpec.from_dsl_file(relative('../test_data/tiny_test_spec.rb'))
  #   end
  
	it 'should build a spec from a DSL file (test_spec)' do
    @test_spec = GuiSpec.from_dsl_file(relative('../test_data/test_spec.rb'))
   #  clear_views = UmmClearView.generate_clear_views_from_umm(umm)
   #  # Apply spec actions normally taken at end of GuiSpec::Spec.from_files(...)
	  # GuiSpec::SpecDSL.spec.views.each { |key, view| view.apply_global_spec_modifiers } if SpecDSL.global_spec_modifiers&.any?
	  # GuiSpec::SpecDSL.spec.resolve_view_refs
	  # GuiSpec::SpecDSL.clear_global_spec_modifiers
    
    people_collection = @test_spec.retrieve_view(:PeopleCollection, People::Person, :Collection)
    expect(people_collection.columns.first.search_filter[:field_type]).to eq(:complex_attribute)
    
    people_organizer = @test_spec.retrieve_view(:PeopleOrganizer, People::Person, :Organizer)
    expect(people_organizer.content[0]).to be_a(Gui::Widgets::Text)


    hidden_widget_strings = ['Inspection Completed', 'vin']

    car_summary_organizer = @test_spec.retrieve_view(:SmallSummary, Automotive::Car, :Organizer)
    car_summary_organizer.content.each do |w|
      if hidden_widget_strings.include?(w.getter) || hidden_widget_strings.include?(w.label)
        expect(w.hidden).to be true
      end
    end
    # Test manually sorted Organizer content ordering
    car_summary_organizer = @test_spec.retrieve_view(:SmallSummary, Automotive::HybridVehicle, :Organizer)
    expect(car_summary_organizer.content[0].getter).to eq('vin')
    expect(car_summary_organizer.content[1].getter).to eq('miles_per_gallon')
    expect(car_summary_organizer.content[2].getter).to eq('state')
    expect(car_summary_organizer.content[3].getter).to eq('inspection_completed')
    
    # Test collection with columns defined explicitly
    car_summary_collection = @test_spec.retrieve_view(:SummaryExplicitCollection, Automotive::Car, :Collection)
    expect(car_summary_collection.content[0].getter).to eq('miles_per_gallon')
    expect(car_summary_collection.content[1].getter).to eq('state')
    expect(car_summary_collection.content[2].getter).to eq('inspection_completed')
    expect(car_summary_collection.content[3].getter).to eq('vin')
    # Note that 'vin' and 'inspection_completed' are not present, since those widgets are globally hidden
    expect(car_summary_collection.columns.map(&:getter)).to match_array(['miles_per_gallon', 'state'])

	end

  context 'car_example specs' do
    before :all do
      # Must set :spec to new GuiSpec::Spec in order to avoid continuing to use @test_spec
      @car_example_spec = GuiSpec.from_dsl_file(relative('../test_data/car_example_spec.rb'), :spec => GuiSpec::Spec.new)
    end

    # it 'should be able to load additional DSL files into the same spec (car_example_custom_spec)' do
    #   $debug = true
    #   ce_spec = GuiSpec.from_dsl_file(relative('../test_data/report_test_spec.rb'), :spec => @car_example_spec)
    # end

    # TODO there should be a method that retrieves views from whatever specs are loaded.  Should not call the method on whatever variable 
    it 'should be able to load additional DSL files into the same spec (car_example_custom_spec)' do
      GuiSpec.from_dsl_file(relative('../test_data/car_example_custom_spec.rb'), :spec => @car_example_spec)
      # puts @car_example_spec.inspect
      #  clear_views = UmmClearView.generate_clear_views_from_umm(umm)
      #  # Apply spec actions normally taken at end of GuiSpec::Spec.from_files(...)
      # GuiSpec::SpecDSL.spec.views.each { |key, view| view.apply_global_spec_modifiers } if SpecDSL.global_spec_modifiers&.any?
      # GuiSpec::SpecDSL.spec.resolve_view_refs
      # GuiSpec::SpecDSL.clear_global_spec_modifiers
      
      # Test manually sorted Organizer content ordering
      hybrid_vehicle_spec = @car_example_spec.retrieve_view(:Details, Automotive::HybridVehicle, :Organizer)
      # Drivers widget should be first
      expect(hybrid_vehicle_spec.content[0].getter).to eq('drivers')
    end
  end
end
