require 'rspec'
require 'rainbow'
require 'lodepath'
LodePath.amend
LodePath.display
require 'uml_metamodel'
# require 'sequel_gen'
require 'gui_spec'
require 'car_example'
require 'sequel_specific_associations'
require 'sequel/extensions/inflector'
require 'sequel_change_tracker'
require 'sqlite3'
DB = Sequel.sqlite
require 'car_example_generated'
require 'car_example_generated/model/models'
require 'car_example_generated/model/schema'
require 'car_example_generated/model/enumeration_instances'
require 'car_example/populate_data'
require 'car_example/model_extensions'

def load_carexample_models
  dsl_file = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
  # This doesn't work! Todo: fix foundation so it does. -SD
  #dsl_file = 'car_example_generated/model/uml.rb'.find_on_load_path
  # This breaks SequelBuilder! Todo: figure out why this won't work -SD
  # dsl_file = '/Users/sdana/.rvm/gems/ruby-2.4.4@gui/gems/car_example_generated-2.7.1/lib/car_example_generated/model/uml.rb'
  umm = UmlMetamodel.from_dsl(File.read(dsl_file))
  

  # Old way, generating Sequel files from UMM. Can't do this because model_extensions don't get loaded.
  # First generate and load Ruby Sequel files, since clear views will need those constants to be defined.
  #SequelBuilder.generate_ruby_sequel_from_umm_project(umm)
  # NOTE: PrometheusFiles module is defined in umm_driven_builder, which is required by umm_sequel_gen
  #model_dir = File.join(PrometheusFiles.root_dir, 'car_example_generated', 'lib', 'car_example_generated', 'model')
  #models = File.join(model_dir, 'models.rb')
  #schema = File.join(model_dir, 'schema.rb')
  #require 'sequel'
  #require 'sqlite3'
  #require 'sequel_specific_associations'
  #require 'sequel/extensions/inflector'
  #require 'sequel_change_tracker'

  # application_module = Foundation.setup_app('car_example')





  # Kernel.const_set(:DB, Sequel.sqlite)
  # load models
  # load schema
end